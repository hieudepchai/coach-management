﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BUS;
using DTO;

namespace GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    { 
        private Home UC_Home = new Home();
        private trips UC_trips = new trips();
        private UserControl tempBack;
        private UserControl tempForward;
        private List<UserControl> ListUserControl = new List<UserControl>();
        private int index = 0;
        BUS_Passenger bustest = new BUS_Passenger();

    
        public void setTempBack(UserControl tempback)
        {
            this.tempBack = tempback;
        }

        public void setTempForward(UserControl tempforward)
        {
            this.tempForward = tempforward;
        }

        public void setListUserControl(UserControl usercontrol)
        {
            if (this.ListUserControl.Count == 0)
                this.ListUserControl.Add(usercontrol);
            else if (index == this.ListUserControl.Count - 1)
            {
                this.ListUserControl.Add(usercontrol);
                index++;
                Icon_forward.Foreground = Brushes.Gray;
            }
            else
            {
                this.ListUserControl.RemoveRange(index + 1, this.ListUserControl.Count - index - 1);
                this.ListUserControl.Add(usercontrol);
                index++;
                Icon_forward.Foreground = Brushes.Gray;
            }
        }

        public List<UserControl> GetListUserContorl()
        {
            return this.ListUserControl;
        }

        public void ShowTrips(trips UC_trip) // show User Control\trips.xaml
        {
            MainTab.Children.Clear();
            MainTab.Children.Add(UC_trip);
            
        }
        public void ShowHome() //show User Control\Home.xaml
        {
            
            MainTab.Children.Clear();
            MainTab.Children.Add(UC_Home);
            
        }

        public void ShowUC_manager() //show User Control\manager.xaml
        {
            MainTab.Children.Clear();

        }

        public MainWindow()
        {           
            InitializeComponent();            
                    
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            UC_Home.setWindow(this);
            setListUserControl((UserControl)UC_Home);
            ShowHome();
                 
        }

        private void Bt_signUp_Click(object sender, RoutedEventArgs e)
        {
            //test function add passenger! OK!
            //DTO_Passenger p1 = new DTO_Passenger("Dung",1998,"0817917792","2018-01-02");
           // DTO_Passenger p2 = new DTO_Passenger("Hieu", 1998, "0000000", "2018-02-06");
            //bustest.deletePassenger("0000000");
        }

        public void ChangeForegroundBackForward() //when having a UI which is forwarding
        {
            Icon_back.Foreground = Brushes.White;
        }

        
        public void Bt_back_Click(object sender, RoutedEventArgs e)
        {
            index--;
            if (index < 0)
            {
                index = 0;
                return;
            }
            else if (index == 0)
            {
                Icon_back.Foreground = Brushes.Gray;
                Icon_forward.Foreground = Brushes.White;
                tempBack = this.ListUserControl[index];
            }
            else
            {
                Icon_forward.Foreground = Brushes.White;
                tempBack = this.ListUserControl[index];
            }
            
            MainTab.Children.Clear();
            MainTab.Children.Add(tempBack);
            
        }

        public void Bt_forward_Click(object sender, RoutedEventArgs e)
        {
            index++;
            if (index >= this.ListUserControl.Count)
            {
                index = this.ListUserControl.Count - 1;
                return;
            }
            else if(index == this.ListUserControl.Count - 1)
            {
                Icon_back.Foreground = Brushes.White;
                Icon_forward.Foreground = Brushes.Gray;
                tempForward = this.ListUserControl[index];

            }
            else
            {
                Icon_back.Foreground = Brushes.White;
                tempForward = this.ListUserControl[index];
            }
            MainTab.Children.Clear();
            MainTab.Children.Add(tempForward);
            
        }

    }
}
