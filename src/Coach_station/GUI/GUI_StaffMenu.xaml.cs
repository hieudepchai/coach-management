﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GUI.User_control;

namespace GUI
{
    /// <summary>
    /// Interaction logic for GUI_StaffMenu.xaml
    /// </summary>
    public partial class GUI_StaffMenu : Window
    {
        private UC_TripList uc_triplist = new UC_TripList();
        private UC_SeatMap uc_seatmap = new UC_SeatMap();
        private UC_Trip_Schedule uc_tripschedule = new UC_Trip_Schedule();
        private Ticket_analysis UC_Ticketana = new Ticket_analysis();
        private Finance_analysis UC_Financeana = new Finance_analysis();

        private UC_Manage_Customer UC_ManPassen = new UC_Manage_Customer();
        private UC_Manage_Bus UC_ManBus = new UC_Manage_Bus();
        private UC_Manage_Admin UC_ManAdmin = new UC_Manage_Admin();
        private UC_Booking UC_Book = new UC_Booking();
        private UC_Manage_Bonus UC_Bonus = new UC_Manage_Bonus();

        private Login GUI_Login = new Login();

        //private UserControl tempBack;
        //private UserControl tempForward;
        //private List<UserControl> ListUC = new List<UserControl>();
        //public void setTempBack(UserControl tempback)
        //{
        //    this.tempBack = tempback;
        //}

        //public void setTempForward(UserControl tempforward)
        //{
        //    this.tempForward = tempforward;
        //}
        public void loadUC_TripList(UC_TripList  in_uc_triplist)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(in_uc_triplist);
        }
        //public void addListUC(UserControl in_uc)
        //{
        //    ListUC.Add(in_uc);
        //}
        public void loadUC_SeatMap(UC_SeatMap in_uc_seatmap)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(in_uc_seatmap);
        }
        private void staffmenuWindowLoaded(object sender, RoutedEventArgs e)
        {
            uc_triplist.setStaffMenu(this);
            //uc_seatmap.setStaffMenu(this);
            //addListUC(uc_triplist);

            GUI_Login.ShowDialog();
        }

        //private void btn_switch_forward(object sender, RoutedEventArgs e)
        //{
        //    btnUCFoward.Visibility = Visibility.Hidden;
        //    btnUCBack.Visibility = Visibility.Visible;
        //    GridMain.Children.Clear();
        //    GridMain.Children.Add(uc_seatmap);
        //}

        private void btn_switch_back(object sender, RoutedEventArgs e)
        {
            //btnUCFoward.Visibility = Visibility.Visible;
            btnUCBack.Visibility = Visibility.Hidden;
            GridMain.Children.Clear();
            GridMain.Children.Add(uc_triplist);
        }
        public void show_btnUCBack()
        {
            btnUCBack.Visibility = Visibility.Visible;
        }

        public GUI_StaffMenu()
        {
            InitializeComponent();
        }
        private void TripListMouseDown(object sender, MouseButtonEventArgs e)
        {
            UC_TripList new_uc_triplist = new UC_TripList();
            new_uc_triplist.setStaffMenu(this);
            GridMain.Children.Clear();
            GridMain.Children.Add(new_uc_triplist);
        }

        private void MouseDown_Drag(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void btnClose_click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();

        }

        private void btnClose_mouseenter(object sender, MouseEventArgs e)
        {
            btnClose.Background = Brushes.Red;
        }

        private void btnClose_MouseLeave(object sender, MouseEventArgs e)
        {
            btnClose.Background = Brushes.Transparent;
        }

        private void btnMiMax_click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
            }
            else
                this.WindowState = WindowState.Normal;
        }

        private void btnMin_click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Minimized;
            }
            else
                this.WindowState = WindowState.Normal;
        }
        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
            if (TripTaskPanel.Visibility == Visibility.Visible)
                TripTaskPanel.Visibility = Visibility.Collapsed;
            if (dbtaskPanel.Visibility == Visibility.Visible)
                dbtaskPanel.Visibility = Visibility.Collapsed;
            if (reporttaskPanel.Visibility == Visibility.Visible)
                reporttaskPanel.Visibility = Visibility.Collapsed;
        }

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //UserControl usc = null;
            //GridMain.Children.Clear();

            //switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            //{
            //    case "ItemHome":
            //        usc = new UserControlHome();
            //        GridMain.Children.Add(usc);
            //        break;
            //    case "ItemCreate":
            //        usc = new UserControlCreate();
            //        GridMain.Children.Add(usc);
            //        break;
            //    default:
            //        break;
            //}
        }

        private void Panel_MouseEnter(object sender, MouseEventArgs e)
        {
            StackPanel thisPanel = e.Source as StackPanel;
            thisPanel.Background = (Brush)new BrushConverter().ConvertFrom("#26111111");
        }

        private void Panel_MouseLeave(object sender, MouseEventArgs e)
        {
            StackPanel thisPanel = e.Source as StackPanel;
            thisPanel.Background = (Brush)new BrushConverter().ConvertFrom("#FF31577E");
        }

        private void tripPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (TripTaskPanel.Visibility == Visibility.Collapsed)
            {
                TripTaskPanel.Visibility = Visibility.Visible;
            }
            else
                TripTaskPanel.Visibility = Visibility.Collapsed;

        }

        private void topbtnMousenter(object sender, MouseEventArgs e)
        {
            Button btn = e.Source as Button;
            btn.Background = (Brush)new BrushConverter().ConvertFrom("#10111111");
        }

        private void topbtnMouseLeave(object sender, MouseEventArgs e)
        {
            Button btn = e.Source as Button;
            btn.Background = Brushes.Transparent;
        }

        private void dbPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (dbtaskPanel.Visibility == Visibility.Collapsed)
            {
                dbtaskPanel.Visibility = Visibility.Visible;
            }
            else
                dbtaskPanel.Visibility = Visibility.Collapsed;
        }

        private void reportPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (reporttaskPanel.Visibility == Visibility.Collapsed)
            {
                reporttaskPanel.Visibility = Visibility.Visible;
            }
            else
                reporttaskPanel.Visibility = Visibility.Collapsed;
        }

        private void Bt_TripPanel_Click(object sender, RoutedEventArgs e)
        {
            if (TripTaskPanel.Visibility == Visibility.Collapsed)
            {
                TripTaskPanel.Visibility = Visibility.Visible;
            }
            else
                TripTaskPanel.Visibility = Visibility.Collapsed;
            if (ButtonOpenMenu.Visibility == Visibility.Visible)
            {
                ButtonCloseMenu.Visibility = Visibility.Visible;
                ButtonOpenMenu.Visibility = Visibility.Collapsed;
            }
        }

        private void Bt_reportPanel_Click(object sender, RoutedEventArgs e)
        {
            if (reporttaskPanel.Visibility == Visibility.Collapsed)
            {
                reporttaskPanel.Visibility = Visibility.Visible;
            }
            else
                reporttaskPanel.Visibility = Visibility.Collapsed;
            if (ButtonOpenMenu.Visibility == Visibility.Visible)
            {
                ButtonCloseMenu.Visibility = Visibility.Visible;
                ButtonOpenMenu.Visibility = Visibility.Collapsed;
            }
        }

        private void Bt_dbPanel_Click(object sender, RoutedEventArgs e)
        {
            if (dbtaskPanel.Visibility == Visibility.Collapsed)
            {
                dbtaskPanel.Visibility = Visibility.Visible;
            }
            else
                dbtaskPanel.Visibility = Visibility.Collapsed;
            if (ButtonOpenMenu.Visibility == Visibility.Visible)
            {
                ButtonCloseMenu.Visibility = Visibility.Visible;
                ButtonOpenMenu.Visibility = Visibility.Collapsed;
            }
        }

        private void TicketMouseDown(object sender, MouseButtonEventArgs e)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(UC_Ticketana);
        }

        private void FinanceMouseDown(object sender, MouseButtonEventArgs e)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(UC_Financeana);
        }

        private void ManPassengerMouseDown(object sender, MouseButtonEventArgs e)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(UC_ManPassen);
        }

        private void ManBusMouseDown(object sender, MouseButtonEventArgs e)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(this.UC_ManBus);
        }

        private void ManBonusMouseDown(object sender, MouseButtonEventArgs e)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(this.UC_Bonus);
        }

        private void ManAdminMouseDown(object sender, MouseButtonEventArgs e)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(this.UC_ManAdmin);
        }
        private void TripScheduleMouseDown(object sender, MouseButtonEventArgs e)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(this.uc_tripschedule);
        }

        private void BookingMouseDown(object sender, MouseButtonEventArgs e)
        {
            GridMain.Children.Clear();
            GridMain.Children.Add(this.UC_Book);
        }




        //public void LoadTripListFrame()
        //{
        //    BorderLoad.Child = new UC_TripList();
        //}

        //private void adj_nav(object sender, RoutedEventArgs e)
        //{
        //    double current = iconcolumn.ActualWidth;
        //    if (current > 50)
        //    {
        //        iconcolumn.Width = new GridLength(50);
        //    }
        //    else
        //        iconcolumn.Width = new GridLength(2,GridUnitType.Star);
        //}
    }
}
