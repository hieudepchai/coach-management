﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DTO;
using BUS;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        BUS_Account account = new BUS_Account();

        public Login()
        {
            InitializeComponent();
        }

        private void btnClose_click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void btnClose_MouseLeave(object sender, MouseEventArgs e)
        {
            btnClose.Background = (Brush)new BrushConverter().ConvertFrom("#FFDCC0C0");
        }


        private void Button_Login_Click(object sender, RoutedEventArgs e)
        {
            if (account.checkAccount(tbx_username.Text, tbx_password.Password.ToString()) != null)
            { //MessageBox.Show("Login Successfull! You will direct to staff");
                //GUI_StaffMenu UI_menu = new GUI_StaffMenu();
                //App.Current.MainWindow = UI_menu;
                this.Close();
                //UI_menu.Show();
            }
            else MessageBox.Show("Fail to login!");
        }

        private void Button_Cancel_Click_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

    }
}
