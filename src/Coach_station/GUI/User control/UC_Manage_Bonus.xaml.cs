﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BUS;
using DTO;

namespace GUI.User_control
{
    /// <summary>
    /// Interaction logic for UC_Manage_Bonus.xaml
    /// </summary>
    public partial class UC_Manage_Bonus : UserControl
    {
        BUS_Bonus bonus = new BUS_Bonus();
        int function = 0;
        private MainWindow window;
        public UC_Manage_Bonus()
        {
            InitializeComponent();
            BonusGrid.ItemsSource = bonus.getBonus().DefaultView;
            btn_save.IsEnabled = false;
        }
        public int isNotEmpty()
        {
            if (tbx_staffid.Text == "") return 0;
            if (tbx_bonus.Text == "") return 0;
            if (tbx_date.Text == "") return 0;
            if (tbx_description.Text == "") return 0;
            return 1;
        }
        public void enableButton(Boolean t)
        {
            btn_add.IsEnabled = t;
            btn_delete.IsEnabled = t;
            btn_edit.IsEnabled = t;
            btn_save.IsEnabled = t;
        }
        public void enableTextBox(Boolean t)
        {
            tbx_staffid.IsEnabled = t;
            tbx_id.IsEnabled = t;
            tbx_bonus.IsEnabled = t;
            tbx_description.IsEnabled = t;
            tbx_bonus.IsEnabled = t;
        }
        public void EndFunction()
        {
            clearTextBox();
            enableButton(true);
            enableTextBox(true);
            btn_save.IsEnabled = false;
            BonusGrid.ItemsSource = bonus.getBonus().DefaultView;
            function = 0;

        }
        public void clearTextBox()
        {
            tbx_staffid.Text = "";
            tbx_id.Text = "";
            tbx_description.Text = "";
            tbx_date.Text = "";
            tbx_bonus.Text = "";
        }

        private void BonusGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearTextBox();
            DataGrid dg = (DataGrid)sender;
            DataRowView row_selected = dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {
                tbx_staffid.Text = row_selected["StaffID"].ToString();
                tbx_id.Text = row_selected["BonusID"].ToString();
                tbx_bonus.Text = row_selected["BonusAmount"].ToString();
                tbx_date.Text = row_selected["ValidDate"].ToString();
                tbx_description.Text = row_selected["Description"].ToString();

            }

        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            clearTextBox();
            function = 1;
            btn_save.IsEnabled = true;
            tbx_id.IsEnabled = false;

        }

        private void btn_edit_Click(object sender, RoutedEventArgs e)
        {
            btn_save.IsEnabled = true;
            tbx_id.IsEnabled = false;
            function = 3;
            tbx_staffid.IsEnabled = false;
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            tbx_id.IsEnabled = false;
            function = 2;
            btn_save.IsEnabled = true;

        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            switch (function)
            {
                case 1:
                    {
                        if (isNotEmpty() == 1)
                        {

                            DTO_Bonus dtbonus = new DTO_Bonus(Int32.Parse(tbx_staffid.Text), float.Parse(tbx_bonus.Text), tbx_date.Text, tbx_description.Text);

                            if (bonus.addBonus(dtbonus) == false) MessageBox.Show("Failed to insert bonus !!!");
                            else
                            {
                                MessageBox.Show("Done!!!");
                            }
                            EndFunction();
                        }
                        else
                        {
                            MessageBox.Show("No empty content allowed!!!");
                        }
                        break;
                    }
                case 2:
                    {

                        {
                            if (tbx_id.Text != "")
                            {

                                if (bonus.deleteBonus(Int32.Parse(tbx_id.Text)) == 0) MessageBox.Show("Failed to delete bonus!!!");
                                else MessageBox.Show("Done!!!");
                            }
                            else
                            {
                                MessageBox.Show("ID must have value!!!");
                                EndFunction();
                            }
                            break;
                        }
                    }
                case 3:
                    {
                        DTO_Bonus dtbonus = new DTO_Bonus(Int32.Parse(tbx_id.Text), Int32.Parse(tbx_staffid.Text), float.Parse(tbx_bonus.Text), tbx_date.Text, tbx_description.Text);
                        if (bonus.editBonus(dtbonus) == 0) MessageBox.Show("Fail to update Bonus!!!");
                        else
                        {
                            MessageBox.Show("Done!!!");
                            EndFunction();
                        }
                        break;
                    }
            }
        }

        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            enableTextBox(true);
            enableButton(true);
            clearTextBox();
            btn_save.IsEnabled = false;
            BonusGrid.ItemsSource = bonus.getBonus().DefaultView;

        }
    }
}
