﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BUS;
using DTO;

namespace GUI.User_control
{
    /// <summary>
    /// Interaction logic for UC_Booking.xaml
    /// </summary>
    public partial class UC_Booking : UserControl
    {
        BUS_Booking booking = new BUS_Booking();
        int function = 0;
        private MainWindow window;
        public UC_Booking()
        {
            InitializeComponent();
            BookingGrid.ItemsSource = booking.getBooking().DefaultView;
            btn_save.IsEnabled = false;
        }
        public void setWindow(MainWindow window)
        {
            this.window = window;
        }
        public int isNotEmpty()
        {
            if (tbx_date.Text == "") return 0;
            if (tbx_seatno.Text == "") return 0;
            if (tbx_passengerid.Text == "") return 0;
            return 1;
        }
        public void enableButton(Boolean t)
        {
            btn_add.IsEnabled = t;
            btn_delete.IsEnabled = t;
            btn_edit.IsEnabled = t;
            btn_save.IsEnabled = t;
        }

        public void enableTextBox(Boolean t)
        {
            tbx_tripid.IsEnabled = t;
            tbx_passengerid.IsEnabled = t;
            tbx_seatno.IsEnabled = t;
            tbx_tripid.IsEnabled = t;
        }
        public void EndFunction()
        {
            clearTextBox();
            enableButton(true);
            enableTextBox(true);
            btn_save.IsEnabled = false;
            BookingGrid.ItemsSource = booking.getBooking().DefaultView;
            function = 0;
        }

        public void clearTextBox()
        {
            tbx_tripid.Text = "";
            tbx_seatno.Text = "";
            tbx_passengerid.Text = "";
            tbx_date.Text = "";
        }


        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            clearTextBox();
            function = 1;
            btn_save.IsEnabled = true;
            tbx_tripid.IsEnabled = false;

        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            switch (function)
            {
                case 1:
                    {
                        if (isNotEmpty() == 1)
                        {

                            DTO_Booking dtbooking = new DTO_Booking(Int32.Parse(tbx_seatno.Text), Int32.Parse(tbx_passengerid.Text), tbx_date.Text);

                            if (booking.addBooking(dtbooking) == false) MessageBox.Show("Failed to insert booking !!!");
                            else
                            {
                                MessageBox.Show("Done!!!");
                            }
                            EndFunction();
                        }
                        else
                        {
                            MessageBox.Show("No empty content allowed!!!");
                        }
                        break;
                    }
                case 2:
                    {

                        {
                            if (tbx_tripid.Text != "")
                            {

                                if (booking.deleteBooking(Int32.Parse(tbx_tripid.Text)) == 0) MessageBox.Show("Failed to delete bus!!!");
                                else MessageBox.Show("Done!!!");
                            }
                            else
                            {
                                MessageBox.Show("ID must have value!!!");
                                EndFunction();
                            }
                            break;
                        }
                    }
                case 3:
                    {
                        DTO_Booking dtbooking = new DTO_Booking(Int32.Parse(tbx_tripid.Text), Int32.Parse(tbx_seatno.Text), Int32.Parse(tbx_passengerid.Text), tbx_date.Text);
                        if (booking.editBooking(dtbooking) == 0) MessageBox.Show("Fail to update Booking!!!");
                        else
                        {
                            MessageBox.Show("Done!!!");
                            EndFunction();
                        }
                        break;
                    }

            }


        }
        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearTextBox();
            DataGrid dg = (DataGrid)sender;
            DataRowView row_selected = dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {
                tbx_tripid.Text = row_selected["TripID"].ToString();
                tbx_seatno.Text = row_selected["SeatNo"].ToString();
                tbx_passengerid.Text = row_selected["PassengerID"].ToString();
                tbx_date.Text = row_selected["CreatedDate"].ToString();

            }

        }

        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            enableTextBox(true);
            enableButton(true);
            clearTextBox();
            btn_save.IsEnabled = false;
            BookingGrid.ItemsSource = booking.getBooking().DefaultView;

        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            tbx_tripid.IsEnabled = false;
            function = 2;
            btn_save.IsEnabled = true;

        }

        private void btn_edit_Click(object sender, RoutedEventArgs e)
        {
            btn_save.IsEnabled = true;
            tbx_tripid.IsEnabled = false;
            function = 3;

        }
    }
}