﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using BUS;
using DTO;
namespace GUI.User_control
{
    /// <summary>
    /// Interaction logic for UC_Trip_Schedule.xaml
    /// </summary>
    public partial class UC_Trip_Schedule : UserControl
    {
        BUS_Staff bus_staff = new BUS_Staff();
        BUS_Pair bus_pair = new BUS_Pair();
        BUS_Bus bus_bus = new BUS_Bus();
        BUS_Trip bus_trip = new BUS_Trip();
        public UC_Trip_Schedule()
        {
            InitializeComponent();
            loadDriverToDG();
            loadPairtoDG();
            loadBusToDg();
        }
        public void loadDriverToDG()
        {
            dgDriver.ItemsSource = bus_staff.getDriver().DefaultView;
        }
        public void loadPairtoDG()
        {
            dgPair.ItemsSource = bus_pair.getPair().DefaultView;
        }
        public void loadBusToDg()
        {
            dgBUS.ItemsSource = bus_bus.getBus().DefaultView;
        }

        private void dgDriver_loadcell(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            DataRowView row_selected = dg.SelectedItem as DataRowView;
            if (row_selected !=null)
            {
                tbDriverID.Text = row_selected["StaffID"].ToString();
            }
        }

        private void dgBus_loadcell(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            DataRowView row_selected = dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {
                tbBusID.Text = row_selected["BusID"].ToString();
            }

        }

        private void dgPair_loadcel(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            DataRowView row_selected = dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {
                tbPairID.Text = row_selected["PairID"].ToString();
                tbDriverID.Text = row_selected["StaffID"].ToString();
                tbBusID.Text = row_selected["BusID"].ToString();
            }
      
        }

        private void btnSave(object sender, RoutedEventArgs e)
        {
            //List<Tuple<string, string>> TupleList = bus_pair.getTupleList();
            //bus_trip.generateDailyTrips("2019/01/19");
            if (tbBusID.Text == "" || tbDriverID.Text == "" || tbPairID.Text == "")
            {
                MessageBox.Show("Not empty input");
            }
            else {
                DTO_Pair pair = new DTO_Pair(Int32.Parse(tbPairID.Text), Int32.Parse(tbDriverID.Text), Int32.Parse(tbBusID.Text));
                int res = bus_pair.editPair(pair);
                if (res > 0)
                {
                    MessageBox.Show("Success");
                    loadPairtoDG();
                }
                else
                {
                    MessageBox.Show("Failed");
                }
            }
          
        }
    }
}
