﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BUS;
using DTO;

namespace GUI
{
    /// <summary>
    /// Interaction logic for UC_Manage_Admin.xaml
    /// </summary>
    public partial class UC_Manage_Admin : UserControl
    {
        BUS_Staff BUSstaff = new BUS_Staff();
        BUS_Account BUSaccount = new BUS_Account();
        int function = 0;
        public UC_Manage_Admin()
        {
            InitializeComponent();
            StaffGrid.ItemsSource = BUSstaff.getStaff().DefaultView;
            btn_save.IsEnabled = false;

        }
        public void clearTextBox()
        {
            tbx_id.Text = "";
            tbx_username.Text = "";
            tbx_password.Clear();
            tbx_fullname.Text = "";
            tbx_phonenum.Text = "";
            tbx_address.Text = "";
            tbx_posid.Text = "";
            tbx_dob.Text = "";
            tbx_salary.Text = "";
            tbx_startdate.Text = "";

        }

        public int isStaffNotEmpty()
        {
            if (tbx_username.Text == "") return 0;
            if (tbx_password.Password == "") return 0;
            if (tbx_fullname.Text == "") return 0;
            if (tbx_phonenum.Text == "") return 0;
            if (tbx_address.Text == "") return 0;
            if (tbx_posid.Text == "") return 0;
            if (tbx_dob.Text == "") return 0;
            if (tbx_salary.Text == "") return 0;
            if (tbx_startdate.Text == "") return 0;
            return 1;
        }
        public void enableButton(Boolean t)
        {
            btn_addstaff.IsEnabled = t;
            btn_delete.IsEnabled = t;
            btn_edit.IsEnabled = t;
            btn_save.IsEnabled = t;
        }
        public void enableTextBox(Boolean t)
        {
            tbx_id.IsEnabled = t;
            tbx_username.IsEnabled = t;
            tbx_password.IsEnabled = t;
            tbx_fullname.IsEnabled = t;
            tbx_phonenum.IsEnabled = t;
            tbx_address.IsEnabled = t;
            tbx_posid.IsEnabled = t;
            tbx_dob.IsEnabled = t;
            tbx_salary.IsEnabled = t;
            tbx_startdate.IsEnabled = t;
        }

        public void EndFunction()
        {
            clearTextBox();
            enableButton(true);
            enableTextBox(true);
            btn_save.IsEnabled = false;
            StaffGrid.ItemsSource = BUSstaff.getStaff().DefaultView;
            function = 0;

        }
        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearTextBox();
            DataGrid dg = (DataGrid)sender;
            DataRowView row_selected = dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {
                tbx_id.Text = row_selected["StaffID"].ToString();
                tbx_username.Text = row_selected["Username"].ToString();
                tbx_password.Password = row_selected["Password"].ToString();
                tbx_fullname.Text = row_selected["FullName"].ToString();
                tbx_phonenum.Text = row_selected["PhoneNumber"].ToString();
                tbx_address.Text = row_selected["Address"].ToString();
                tbx_posid.Text = row_selected["PositionID"].ToString();
                tbx_dob.Text = row_selected["DOB"].ToString();
                tbx_salary.Text = row_selected["Salary"].ToString();
            }
        }
        private void btn_addStaff_Click(object sender, RoutedEventArgs e)
        {
            enableTextBox(true);
            clearTextBox();
            tbx_id.IsEnabled = false;
            function = 1;
            btn_save.IsEnabled = true;
        }


        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            enableTextBox(true);
            enableButton(true);
            clearTextBox();
            btn_save.IsEnabled = false;
            StaffGrid.ItemsSource = BUSstaff.getStaff().DefaultView;
        }
        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            switch (function)
            {
                case 1:
                    {
                        if (isStaffNotEmpty() == 1)
                        {
                            int result;
                            DTO_Staff dtstaff = new DTO_Staff(tbx_fullname.Text, tbx_dob.Text, tbx_phonenum.Text, tbx_address.Text, Int32.Parse(tbx_posid.Text), tbx_startdate.Text, float.Parse(tbx_salary.Text));
                            if (BUSstaff.addStaff(dtstaff) == false) MessageBox.Show("Failed to insert!!!");
                            else MessageBox.Show("Add staff done!");
                            DTO_Account dtaccount = new DTO_Account(tbx_username.Text, tbx_password.Password, BUSstaff.getStaffID(tbx_fullname.Text));
                            if (BUSaccount.addAccount(dtaccount) == false) MessageBox.Show("Failed to insert account!!!");
                            else MessageBox.Show("Add account done!");
                        EndFunction();
                        }
                        else
                        {
                            MessageBox.Show("No empty content allowed!!!");
                        }
                        break;
                    }
                case 2:
                    {

                        {
                            if (tbx_id.Text != "")
                            {

                                if (BUSaccount.deleteAccount(Int32.Parse(tbx_id.Text)) == 0) MessageBox.Show("Failed to delete!!!");
                                else MessageBox.Show("Delete Account Done!!!");
                                if (BUSstaff.deleteStaff(Int32.Parse(tbx_id.Text)) == 0) MessageBox.Show("Failed to delete!!!");
                                else MessageBox.Show("Delete Staff Done!!!");
                            }
                            else
                            {
                                MessageBox.Show("Phone Number must have value!!!");
                                EndFunction();
                            }
                            break;
                        }
                    }
                case 3:
                    {
                        DTO_Account dtaccount = new DTO_Account(tbx_username.Text, tbx_password.Password, Int32.Parse(tbx_id.Text));
                        if (BUSaccount.editAccount(dtaccount) == 0) MessageBox.Show("Fail to update account!!!");
                        else
                            MessageBox.Show("Update Account done!!!!");
                        DTO_Staff dtstaff = new DTO_Staff(Int32.Parse(tbx_id.Text), tbx_fullname.Text, tbx_dob.Text, tbx_phonenum.Text, tbx_address.Text, Int32.Parse(tbx_posid.Text), tbx_startdate.Text, float.Parse(tbx_salary.Text));
                        if (BUSstaff.editStaff(dtstaff) == 0) MessageBox.Show("Fail to update staff!!!");
                        else
                        {
                            MessageBox.Show("Update Staff Done!!!");
                            EndFunction();
                        }
                        break;
                    }

            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            tbx_phonenum.IsEnabled = true;
            function = 2;
            btn_save.IsEnabled = true;

        }

        private void btn_edit_Click(object sender, RoutedEventArgs e)
        {
            btn_save.IsEnabled = true;
            tbx_id.IsEnabled = false;
            function = 3;

        }

    }
}
