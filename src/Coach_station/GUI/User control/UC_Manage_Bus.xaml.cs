﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BUS;
using DTO;

namespace GUI.User_control
{
    /// <summary>
    /// Interaction logic for UC_Manage_Bus.xaml
    /// </summary>
    public partial class UC_Manage_Bus : UserControl
    {
        BUS_Bus bus = new BUS_Bus();
        int function = 0;
        public UC_Manage_Bus()
        {
            InitializeComponent();
            BusGrid.ItemsSource = bus.getBus().DefaultView;
            btn_save.IsEnabled = false;
        }
        public int isNotEmpty()
        {
            if (tbx_boughtdate.Text == "") return 0;
            if (tbx_brandname.Text == "") return 0;
            if (tbx_liscence.Text == "") return 0;
            return 1;
        }
        public void enableButton(Boolean t)
        {
            btn_add.IsEnabled = t;
            btn_delete.IsEnabled = t;
            btn_edit.IsEnabled = t;
            btn_save.IsEnabled = t;
        }
        public void enableTextBox(Boolean t)
        {
            tbx_ID.IsEnabled = t;
            tbx_liscence.IsEnabled = t;
            tbx_brandname.IsEnabled = t;
            tbx_boughtdate.IsEnabled = t;
        }
        public void EndFunction()
        {
            clearTextBox();
            enableButton(true);
            enableTextBox(true);
            btn_save.IsEnabled = false;
            BusGrid.ItemsSource = bus.getBus().DefaultView;
            function = 0;

        }
        public void clearTextBox()
        {
            tbx_ID.Text = "";
            tbx_boughtdate.Text = "";
            tbx_brandname.Text = "";
            tbx_liscence.Text = "";
        }

        private void BusGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearTextBox();
            DataGrid dg = (DataGrid)sender;
            DataRowView row_selected = dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {
                tbx_ID.Text = row_selected["BusID"].ToString();
                tbx_liscence.Text = row_selected["LiscencePlate"].ToString();
                tbx_brandname.Text = row_selected["BrandName"].ToString();
                tbx_boughtdate.Text = row_selected["BoughtDate"].ToString();

            }
        }
        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            switch (function)
            {
                case 1:
                    {
                        if (isNotEmpty() == 1)
                        {

                            DTO_Bus dtpass = new DTO_Bus(tbx_liscence.Text, tbx_brandname.Text, tbx_boughtdate.Text);

                            if (bus.addBus(dtpass) == false) MessageBox.Show("Failed to insert bus !!!");
                            else
                            {
                                MessageBox.Show("Done!!!");
                            }
                            EndFunction();
                        }
                        else
                        {
                            MessageBox.Show("No empty content allowed!!!");
                        }
                        break;
                    }
                case 2:
                    {

                        {
                            if (tbx_ID.Text != "")
                            {

                                if (bus.deleteBus(Int32.Parse(tbx_ID.Text)) == 0) MessageBox.Show("Failed to delete bus!!!");
                                else MessageBox.Show("Done!!!");
                            }
                            else
                            {
                                MessageBox.Show("ID must have value!!!");
                                EndFunction();
                            }
                            break;
                        }
                    }
                case 3:
                    {
                        DTO_Bus dtbus = new DTO_Bus(Int32.Parse(tbx_ID.Text), tbx_liscence.Text, tbx_brandname.Text, tbx_boughtdate.Text);
                        if (bus.editBus(dtbus) == 0) MessageBox.Show("Fail to update bus!!!");
                        else
                        {
                            MessageBox.Show("Done!!!");
                            EndFunction();
                        }
                        break;
                    }

            }
        }
        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            clearTextBox();
            function = 1;
            btn_save.IsEnabled = true;
            tbx_ID.IsEnabled = false;

        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            tbx_ID.IsEnabled = false;
            function = 2;
            btn_save.IsEnabled = true;
        }

        private void btn_edit_Click(object sender, RoutedEventArgs e)
        {
            btn_save.IsEnabled = true;
            tbx_ID.IsEnabled = false;
            function = 3;
        }
        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            enableTextBox(true);
            enableButton(true);
            clearTextBox();
            btn_save.IsEnabled = false;
            BusGrid.ItemsSource = bus.getBus().DefaultView;
        }

    }
}
