﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        private int placeIndex;
        private string date;
        private trips UC_trips;
        private MainWindow window;

        public Home()
        {
            InitializeComponent();           
        }

        public void setWindow(MainWindow window)
        {
            this.window = window;
        }

        //SelectedDate="{Binding Path = SellStartDate, StringFormat = {}{0:dd-MM-yyyy}}"
        private int index = 0;
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            index++;
            if (index == 1)
            {
                DateTime now = DateTime.Now;
                dp_date.Text = now.ToString();
            }
                     
        }

        public int GetPlaceIndex()
        {
            return this.placeIndex;
        }

        public String GetDate()
        {
            return this.date;
        }

        private void Bt_search_Click(object sender, RoutedEventArgs e)
        {
            placeIndex = cb_place.SelectedIndex;
            date = dp_date.Text;

            UC_trips = new trips();
            UC_trips.setPlace(placeIndex);
            UC_trips.setDate(date);

            window.ChangeForegroundBackForward();
            window.setListUserControl((UserControl)UC_trips);
            window.ShowTrips(UC_trips);
        }

       
    }
}
