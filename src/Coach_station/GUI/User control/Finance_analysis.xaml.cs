﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI.User_control
{
    /// <summary>
    /// Interaction logic for Finance_analysis.xaml
    /// </summary>
    public partial class Finance_analysis : UserControl
    {
        BUS.Finance_analysis Fa = new BUS.Finance_analysis();
        List<DTO.Finance_analysis> ListFinancePerTime;
        List<string> ListTimes;
        

        public Finance_analysis()
        {
            InitializeComponent();
        }

        private void SetPoint(List<DTO.Finance_analysis> listFinance, List<string> ListTimes)
        {
            List<string> ListTimeH = new List<string>();
            for (int i = 0; i < listFinance.Count; i++)
                ListTimeH.Add(listFinance[i].GetDepTime());
            for(int i = 0; i < ListTimes.Count; i++)
            {
                if (!ListTimeH.Contains(ListTimes[i]))
                {
                    DTO.Finance_analysis onerow = new DTO.Finance_analysis(0, ListTimes[i]);
                    listFinance.Add(onerow);
                }
            }

            col1.Argument = listFinance[0].GetDepTime();
            col1.Value = listFinance[0].getMoney();

            col2.Argument = listFinance[1].GetDepTime();
            col2.Value = listFinance[1].getMoney();

            col3.Argument = listFinance[2].GetDepTime();
            col3.Value = listFinance[2].getMoney();

            col4.Argument = listFinance[3].GetDepTime();
            col4.Value = listFinance[3].getMoney();

            col5.Argument = listFinance[4].GetDepTime();
            col5.Value = listFinance[4].getMoney();

            col6.Argument = listFinance[5].GetDepTime();
            col6.Value = listFinance[5].getMoney();

            col7.Argument = listFinance[6].GetDepTime();
            col7.Value = listFinance[6].getMoney();

            col8.Argument = listFinance[7].GetDepTime();
            col8.Value = listFinance[7].getMoney();

            col9.Argument = listFinance[8].GetDepTime();
            col9.Value = listFinance[8].getMoney();

            col10.Argument = listFinance[9].GetDepTime();
            col10.Value = listFinance[9].getMoney();

            col11.Argument = listFinance[10].GetDepTime();
            col11.Value = listFinance[10].getMoney();

            col12.Argument = listFinance[11].GetDepTime();
            col12.Value = listFinance[11].getMoney();

            col13.Argument = listFinance[12].GetDepTime();
            col13.Value = listFinance[12].getMoney();

            col14.Argument = listFinance[13].GetDepTime();
            col14.Value = listFinance[13].getMoney();

            col15.Argument = listFinance[14].GetDepTime();
            col15.Value = listFinance[14].getMoney();

            col16.Argument = listFinance[15].GetDepTime();
            col16.Value = listFinance[15].getMoney();

            col17.Argument = listFinance[16].GetDepTime();
            col17.Value = listFinance[16].getMoney();

            col18.Argument = listFinance[17].GetDepTime();
            col18.Value = listFinance[17].getMoney();

            col19.Argument = listFinance[18].GetDepTime();
            col19.Value = listFinance[18].getMoney();

            col20.Argument = listFinance[19].GetDepTime();
            col20.Value = listFinance[19].getMoney();

            col21.Argument = listFinance[20].GetDepTime();
            col21.Value = listFinance[20].getMoney();

            col22.Argument = listFinance[21].GetDepTime();
            col22.Value = listFinance[21].getMoney();

            col23.Argument = listFinance[22].GetDepTime();
            col23.Value = listFinance[22].getMoney();

            col24.Argument = listFinance[23].GetDepTime();
            col24.Value = listFinance[23].getMoney();

            col25.Argument = listFinance[24].GetDepTime();
            col25.Value = listFinance[24].getMoney();

            col26.Argument = listFinance[25].GetDepTime();
            col26.Value = listFinance[25].getMoney();

            col27.Argument = listFinance[26].GetDepTime();
            col27.Value = listFinance[26].getMoney();

            col28.Argument = listFinance[27].GetDepTime();
            col28.Value = listFinance[27].getMoney();

            col29.Argument = listFinance[28].GetDepTime();
            col29.Value = listFinance[28].getMoney();

            col30.Argument = listFinance[29].GetDepTime();
            col30.Value = listFinance[29].getMoney();

            col31.Argument = listFinance[29].GetDepTime();
            col32.Value = listFinance[29].getMoney();

            col32.Argument = listFinance[30].GetDepTime();
            col32.Value = listFinance[30].getMoney();

        }

        private void Bt_Statistic_Click(object sender, RoutedEventArgs e)
        {
            this.ListFinancePerTime = Fa.getnFinance(dp_StartDate.Text, dp_EndDate.Text);
            ListTimes = Fa.getListTimes();
            SetPoint(this.ListFinancePerTime, this.ListTimes);
        }

        private void Bt_Statistic_MouseEnter(object sender, MouseEventArgs e)
        {
            bt_Statistic.Background = Brushes.Gray;
        }

        private void Bt_Statistic_MouseLeave(object sender, MouseEventArgs e)
        {
            bt_Statistic.Background = Brushes.White;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DateTime now = DateTime.Now;
            dp_StartDate.Text = now.ToString();
            dp_EndDate.Text = now.ToString();
            Bt_Statistic_Click(sender, e);
        }
    }
}
