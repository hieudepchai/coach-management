﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DTO;

namespace GUI
{
    /// <summary>
    /// Interaction logic for trips.xaml
    /// </summary>
    public partial class trips : UserControl
    {
        private int placeIndex;
        private string date;
        private Home UC_home;

        private Style St_Label;
        private Style St_Textbox;

        private Border[] Arrtrips = new Border[64];
        private Border[] ArrChooseOrder = new Border[64];

        private List<DTO.TripsInfor> ListTripsInfor;
        private BUS.BUS_Trips BUS_trip = new BUS.BUS_Trips();

        private string DeparturePlace;

        public trips()
        {
            InitializeComponent();
            
        }
        private int index = 0;
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            index++;
            if (lb_departPlace.Text == "Ho Chi Minh City - Ben Tre")
                this.DeparturePlace = "HCMC";
            else
                this.DeparturePlace = "Ben Tre";
            ListTripsInfor = BUS_trip.getListTripsInfor(date, this.DeparturePlace);
            var control = sender as UserControl; //style
            if (control != null)
            {
                St_Label = control.Resources["label_font"] as Style;
                St_Textbox = control.Resources["textbox_style"] as Style;
            }
            if (index == 1)
            {
                lb_departPlace.SelectedIndex = placeIndex;
                dp_date.Text = date;
                DrawTrips(ListTripsInfor);
            }
        }

        public void setUC_Home(Home UC_home)
        {
            this.UC_home = UC_home;
        }
        public void setPlace(int placeIndex)
        {
            this.placeIndex = placeIndex;
        }

        public void setDate(string date)
        {
            this.date = date;
        }

        /*<Border x:Name="trip1" BorderThickness = "0,2,0,0" Height = "100" BorderBrush = "Gainsboro" >
                            <Grid>
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="40"/>
                                    <ColumnDefinition Width="*"/>
                                    <ColumnDefinition Width="1.8*"/>
                                    <ColumnDefinition Width="40"/>
                                    <ColumnDefinition Width="1.3*"/>
                                    <ColumnDefinition Width="1.2*"/>
                                    <ColumnDefinition Width="*"/>
                                </Grid.ColumnDefinitions>
                                <Grid.RowDefinitions>
                                    <RowDefinition Height="*"/>
                                    <RowDefinition Height="*"/>
                                    <RowDefinition Height="0.5*"/>
                                </Grid.RowDefinitions>
                                <materialDesign:PackIcon Kind="AirlineSeatReclineNormal" Grid.Column="3" Grid.Row="0" Grid.RowSpan="2" Width="40" Height="40" HorizontalAlignment="Center" VerticalAlignment="Center"/>
                                <materialDesign:PackIcon Kind="AccessTime" Grid.Column="0" Grid.Row="0" Grid.RowSpan="2" Width="40" Height="40" HorizontalAlignment="Center" VerticalAlignment="Center"/>
                                <Label x:Name="lb_PointTime" Content="7:00 - 10:00" Grid.Column="1" Grid.Row="0" FontFamily="Times New Roman" FontSize="20" FontWeight="Bold" HorizontalAlignment="Left" VerticalAlignment="Bottom"/>
                                <Label x:Name="lb_time" Content="Time: 3 hours" Grid.Column="1" Grid.Row="1" FontFamily="Times New Roman" FontSize="20" HorizontalAlignment="Left" VerticalAlignment="Top" Foreground="#DD6B6B6B"/>
                                <Label x:Name="lb_nSeat" Content="17 Online seats" Grid.Column="4" Grid.Row="0" FontFamily="Times New Roman" FontSize="20" FontWeight="Bold" HorizontalAlignment="Left" VerticalAlignment="Bottom"/>
                                <Label x:Name="lb_TypeBus" Content="(Limosine)" Grid.Column="4" Grid.Row="1" FontFamily="Times New Roman" FontSize="20" HorizontalAlignment="Left" VerticalAlignment="Top" Foreground="#DD6B6B6B"/>
                                <Label x:Name="lb_Price" Content="273,000 vnd" Grid.Column="5" Grid.Row="0" Grid.RowSpan="2" Foreground="#DDF90303" FontFamily="Times New Roman" FontSize="24" FontWeight="Bold" VerticalAlignment="Center"/>
                                <Button x:Name="bt_ordering" Content="Booking" BorderThickness="1.5" Grid.Column="6" Grid.Row="0" Grid.RowSpan="2" VerticalAlignment="Center" Background="#FFFA2929" FontFamily="Times New Roman" FontSize="23" Click="Bt_ordering_Click" BorderBrush="White"/>
                                
                            </Grid>
                        </Border>
                        <Border x:Name="Border_ChooseOrder" Height = "400" Background="Gainsboro" BorderThickness="0,0,0,2" UseLayoutRounding="False" BorderBrush="White" Visibility="Visible">
                            <Grid>
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="1.8*"/>
                                    <ColumnDefinition Width="*"/>
                                    <ColumnDefinition Width="30"/>
                                </Grid.ColumnDefinitions>
                                <Grid Grid.Column="0">
                                    <Grid.RowDefinitions>
                                        <RowDefinition Height="2*"/>
                                        <RowDefinition Height="*"/>
                                    </Grid.RowDefinitions>
                                    <Grid Grid.Row="0">
                                        <Grid.RowDefinitions>
                                            <RowDefinition Height="30"/>
                                            <RowDefinition Height="*"/>
                                        </Grid.RowDefinitions>
                                        <Grid.ColumnDefinitions>
                                            <ColumnDefinition Width="30"/>
                                            <ColumnDefinition Width="*"/>
                                            <ColumnDefinition Width="30"/>
                                        </Grid.ColumnDefinitions>
                                        <Border x:Name="border_ChooseSeat" Grid.Row="1" Grid.Column="1" Background="White">
                                            <Grid>
                                                <Grid.RowDefinitions>
                                                    <RowDefinition Height="15"/>
                                                    <RowDefinition Height="*"/>
                                                    <RowDefinition Height="10"/>
                                                    <RowDefinition Height="*"/>
                                                    <RowDefinition Height="30"/>
                                                    <RowDefinition Height="*"/>
                                                    <RowDefinition Height="10"/>
                                                    <RowDefinition Height="*"/>
                                                    <RowDefinition Height="15"/>
                                                </Grid.RowDefinitions>
                                                <Grid.ColumnDefinitions>
                                                    <ColumnDefinition Width="20"/>
                                                    <ColumnDefinition Width="*"/>
                                                    <ColumnDefinition Width="10"/>
                                                    <ColumnDefinition Width="*"/>
                                                    <ColumnDefinition Width="10"/>
                                                    <ColumnDefinition Width="*"/>
                                                    <ColumnDefinition Width="10"/>
                                                    <ColumnDefinition Width="*"/>
                                                    <ColumnDefinition Width="10"/>
                                                    <ColumnDefinition Width="*"/>
                                                    <ColumnDefinition Width="10"/>
                                                    <ColumnDefinition Width="*"/>
                                                    <ColumnDefinition Width="10"/>
                                                    <ColumnDefinition Width="*"/>
                                                    <ColumnDefinition Width="10"/>
                                                    <ColumnDefinition Width="*"/>
                                                </Grid.ColumnDefinitions>
                                                <Button Grid.Column="1" Content="01" Grid.Row="1" Background="#FFFC5650" BorderBrush="#FFFC5650" />
                                                <Button Grid.Column="1" Content="02" Grid.Row="3" Background="#FFFC5650" BorderBrush="#FFFC5650" />
                                                <Button Grid.Column="1" Content="01" Grid.Row="5" Background="#FFFC5650" BorderBrush="#FFFC5650" />
                                                <Button Grid.Column="1" Content="01" Grid.Row="7" Background="#FFFC5650" BorderBrush="#FFFC5650" />
                                                <Button Grid.Column="3" Content="01" Grid.Row="1" Background="#FFAFFC6E" BorderBrush="#FFFC5650" />
                                                <Button Grid.Column="3" Content="02" Grid.Row="3" Background="#FFFC5650" BorderBrush="#FFFC5650" />
                                                <Button Grid.Column="3" Content="01" Grid.Row="5" Background="#FFFC5650" BorderBrush="#FFFC5650" />
                                                <Button Grid.Column="3" Content="01" Grid.Row="7" Background="#FFFC5650" BorderBrush="#FFFC5650" />
                                            </Grid>
                                        </Border> 
                                    </Grid>
                                    <Grid Grid.Column="0" Grid.Row="1">
                                        <Grid.RowDefinitions>
                                            <RowDefinition Height="20"/>
                                            <RowDefinition Height="30"/>
                                            <RowDefinition Height="*"/>
                                        </Grid.RowDefinitions>
                                        <Grid.ColumnDefinitions>
                                            <ColumnDefinition Width="30"/>
                                            <ColumnDefinition Width="40"/>
                                            <ColumnDefinition Width="*"/>
                                            <ColumnDefinition Width="10"/>
                                            <ColumnDefinition Width="40"/>
                                            <ColumnDefinition Width="*"/>
                                            <ColumnDefinition Width="10"/>
                                            <ColumnDefinition Width="40"/>
                                            <ColumnDefinition Width="*"/>
                                            <ColumnDefinition Width="100"/>
                                        </Grid.ColumnDefinitions>
                                        <Rectangle Grid.Column="1" Grid.Row="1" Fill="White"/>
                                        <Rectangle Grid.Column="4" Grid.Row="1" Fill="#FFADADAD"/>
                                        <Rectangle Grid.Column="7" Grid.Row="1" Fill="#FFFC5650"/>
                                        <Label Content="Vacant seat" Grid.Column="2" Grid.Row="1" Style="{StaticResource label_font}"/>
                                        <Label Content="Selected" Grid.Column="5" Grid.Row="1" Style="{StaticResource label_font}"/>
                                        <Label Content="Booked" Grid.Column="8" Grid.Row="1" Style="{StaticResource label_font}"/>
                                    </Grid>
                                </Grid>
                                <Grid Grid.Column="1">
                                    <Grid.RowDefinitions>
                                        <RowDefinition Height="40"/>
                                        <RowDefinition Height="25"/>
                                        <RowDefinition Height="25"/>
                                        <RowDefinition Height="25"/>
                                        <RowDefinition Height="10"/>
                                        <RowDefinition Height="25"/>
                                        <RowDefinition Height="10"/>
                                        <RowDefinition Height="25"/>
                                        <RowDefinition Height="10"/>
                                        <RowDefinition Height="25"/>
                                        <RowDefinition Height="20"/>
                                        <RowDefinition Height="*"/>
                                    </Grid.RowDefinitions>
                                    <Grid.ColumnDefinitions>
                                        <ColumnDefinition Width="*"/>
                                        <ColumnDefinition Width="1.9*"/>
                                    </Grid.ColumnDefinitions>
                                    <Border x:Name="Border_TimePlace" Grid.Row="0" Grid.Column="0" Grid.ColumnSpan="2" BorderBrush="#FF969696" BorderThickness="0,0,0,2">
                                        <StackPanel Orientation="Horizontal">
                                            <Label x:Name="lb_placeBd" Content="Ho Chi Minh City - Vung Tau" Style="{StaticResource label_font}" FontWeight="Bold"/>
                                            <Label x:Name="lb_timeBd" Content="Date: 12/14/2018" Style="{StaticResource label_font}"/>
                                        </StackPanel>
                                    </Border>
                                    <Label x:Name="lb_selected" Content="Selected seat: " Grid.Column="0" Grid.Row="1" Style="{StaticResource label_font}"/>
                                    <Label x:Name="lb_total" Content="Totals: " Grid.Column="0" Grid.Row="2" Style="{StaticResource label_font}"/>
                                    <Label x:Name="lb_PhoneNum" Content="Phone Number: " Grid.Column="0" Grid.Row="3" Style="{StaticResource label_font}"/>
                                    <Label x:Name="lb_FullName" Content="Your Name: " Grid.Column="0" Grid.Row="5" Style="{StaticResource label_font}"/>
                                    <Label x:Name="lb_email" Content="Email: " Grid.Column="0" Grid.Row="7" Style="{StaticResource label_font}"/>
                                    <Label x:Name="lb_PickingPlace" Content="Picking Place: " Grid.Column="0" Grid.Row="9" Style="{StaticResource label_font}"/>
                                    <Label x:Name="lb_nSelected" Content="A1, A2" Grid.Column="1" Grid.Row="1" Style="{StaticResource label_font}" FontWeight="Bold"/>
                                    <Label x:Name="lb_totalPrice" Content="500,000 vnd" Grid.Column="1" Grid.Row="2" Style="{StaticResource label_font}" FontWeight="Bold"/>
                                    <TextBox x:Name="tb_PhoneN" Grid.Column="1" Grid.Row="3" Style="{StaticResource textbox_style}"/>
                                    <TextBox x:Name="tb_name" Grid.Column="1" Grid.Row="5" Style="{StaticResource textbox_style}"/>
                                    <TextBox x:Name="tb_email" Grid.Column="1" Grid.Row="7" Style="{StaticResource textbox_style}"/>
                                    <TextBox x:Name="tb_PickPlace" Grid.Column="1" Grid.Row="9" Style="{StaticResource textbox_style}"/>
                                    <Button x:Name="bt_book" Content="Booking" Grid.Column="0" Grid.Row="12" Background="#FFFA2929" VerticalAlignment="Top" HorizontalAlignment="Right"/>
                                </Grid>
                            </Grid>
                        </Border>*/        

        private Grid DrawNote() //ve ghi chu
        {
            Grid GridNote = new Grid();
            //create row
            RowDefinition GridRow1 = new RowDefinition();
            GridRow1.Height = new GridLength(20);
            RowDefinition GridRow2 = new RowDefinition();
            GridRow2.Height = new GridLength(30);
            RowDefinition GridRow3 = new RowDefinition();
            GridNote.RowDefinitions.Add(GridRow1);
            GridNote.RowDefinitions.Add(GridRow2);
            GridNote.RowDefinitions.Add(GridRow3);
            //create column
            ColumnDefinition GridCol1 = new ColumnDefinition();
            GridCol1.Width = new GridLength(30);
            ColumnDefinition GridCol2 = new ColumnDefinition();
            GridCol2.Width = new GridLength(40);
            ColumnDefinition GridCol3 = new ColumnDefinition();
            ColumnDefinition GridCol4 = new ColumnDefinition();
            GridCol4.Width = new GridLength(10);
            ColumnDefinition GridCol5 = new ColumnDefinition();
            GridCol5.Width = new GridLength(40);
            ColumnDefinition GridCol6 = new ColumnDefinition();
            ColumnDefinition GridCol7 = new ColumnDefinition();
            GridCol7.Width = new GridLength(10);
            ColumnDefinition GridCol8 = new ColumnDefinition();
            GridCol8.Width = new GridLength(40);
            ColumnDefinition GridCol9 = new ColumnDefinition();
            ColumnDefinition GridCol10 = new ColumnDefinition();
            GridCol10.Width = new GridLength(100);
            GridNote.ColumnDefinitions.Add(GridCol1);
            GridNote.ColumnDefinitions.Add(GridCol2);
            GridNote.ColumnDefinitions.Add(GridCol3);
            GridNote.ColumnDefinitions.Add(GridCol4);
            GridNote.ColumnDefinitions.Add(GridCol5);
            GridNote.ColumnDefinitions.Add(GridCol6);
            GridNote.ColumnDefinitions.Add(GridCol7);
            GridNote.ColumnDefinitions.Add(GridCol8);
            GridNote.ColumnDefinitions.Add(GridCol9);
            GridNote.ColumnDefinitions.Add(GridCol10);

            /*<Rectangle Grid.Column="1" Grid.Row="1" Fill="White"/>
              <Rectangle Grid.Column="4" Grid.Row="1" Fill="#FFADADAD"/>
              <Rectangle Grid.Column="7" Grid.Row="1" Fill="#FFFC5650"/>
              <Label Content="Vacant seat" Grid.Column="2" Grid.Row="1" Style="{StaticResource label_font}"/>
              <Label Content="Selected" Grid.Column="5" Grid.Row="1" Style="{StaticResource label_font}"/>
              <Label Content="Booked" Grid.Column="8" Grid.Row="1" Style="{StaticResource label_font}"/>*/
            Rectangle WhiteNote = new Rectangle(); //mau trang
            WhiteNote.Fill = Brushes.White;
            GridNote.Children.Add(WhiteNote);
            Grid.SetColumn(WhiteNote, 1);
            Grid.SetRow(WhiteNote, 1);

            var bc = new BrushConverter();

            Rectangle GreyNote = new Rectangle(); //mau xam
            GreyNote.Fill = (Brush)bc.ConvertFrom("#FFADADAD");
            GridNote.Children.Add(GreyNote);
            Grid.SetColumn(GreyNote, 4);
            Grid.SetRow(GreyNote, 1);

            Rectangle RedNote = new Rectangle(); //mau do
            RedNote.Fill = (Brush)bc.ConvertFrom("#FFFC5650");
            GridNote.Children.Add(RedNote);
            Grid.SetColumn(RedNote, 7);
            Grid.SetRow(RedNote, 1);

            Label WhiteSeat = new Label();
            WhiteSeat.Content = "Vacant seat";
            WhiteSeat.Style = St_Label;
            GridNote.Children.Add(WhiteSeat);
            Grid.SetColumn(WhiteSeat, 2);
            Grid.SetRow(WhiteSeat, 1);

            Label GreySeat = new Label();
            GreySeat.Content = "Selected";
            GreySeat.Style = St_Label;
            GridNote.Children.Add(GreySeat);
            Grid.SetColumn(GreySeat, 5);
            Grid.SetRow(GreySeat, 1);

            Label RedSeat = new Label();
            RedSeat.Content = "Booked";
            RedSeat.Style = St_Label;
            GridNote.Children.Add(RedSeat);
            Grid.SetColumn(RedSeat, 8);
            Grid.SetRow(RedSeat, 1);
            return GridNote;
        }              
               
        private Grid DrawBooking(int TripID, string placeSE, string timeS, Label lb_nSelected, Label lb_totalPrice, List<string> ListSeats, string DeptTime, string DeptPlace, List<Button> btSelect) //adding information of customer
        {
            Grid GridBooking = new Grid();
           
            //create row
            RowDefinition GridRow0 = new RowDefinition();
            GridRow0.Height = new GridLength(40);
            RowDefinition GridRow1 = new RowDefinition();
            GridRow1.Height = new GridLength(25);
            RowDefinition GridRow2 = new RowDefinition();
            GridRow2.Height = new GridLength(25);
            RowDefinition GridRow3 = new RowDefinition();
            GridRow3.Height = new GridLength(25);
            RowDefinition GridRow4 = new RowDefinition();
            GridRow4.Height = new GridLength(10);
            RowDefinition GridRow5 = new RowDefinition();
            GridRow5.Height = new GridLength(25);
            RowDefinition GridRow6 = new RowDefinition();
            GridRow6.Height = new GridLength(10);
            RowDefinition GridRow7 = new RowDefinition();
            GridRow7.Height = new GridLength(25);
            RowDefinition GridRow8 = new RowDefinition();
            GridRow8.Height = new GridLength(10);
            RowDefinition GridRow9 = new RowDefinition();
            GridRow9.Height = new GridLength(25);
            RowDefinition GridRow10 = new RowDefinition();
            GridRow10.Height = new GridLength(20);
            RowDefinition GridRow11 = new RowDefinition();
            GridBooking.RowDefinitions.Add(GridRow0);
            GridBooking.RowDefinitions.Add(GridRow1);
            GridBooking.RowDefinitions.Add(GridRow2);
            GridBooking.RowDefinitions.Add(GridRow3);
            GridBooking.RowDefinitions.Add(GridRow4);
            GridBooking.RowDefinitions.Add(GridRow5);
            GridBooking.RowDefinitions.Add(GridRow6);
            GridBooking.RowDefinitions.Add(GridRow7);
            GridBooking.RowDefinitions.Add(GridRow8);
            GridBooking.RowDefinitions.Add(GridRow9);
            GridBooking.RowDefinitions.Add(GridRow10);
            GridBooking.RowDefinitions.Add(GridRow11);

            //Create column
            ColumnDefinition GridCol0 = new ColumnDefinition();
            ColumnDefinition GridCol1 = new ColumnDefinition();
            GridCol1.Width = new GridLength(2, GridUnitType.Star);
            GridBooking.ColumnDefinitions.Add(GridCol0);
            GridBooking.ColumnDefinitions.Add(GridCol1);

            //adding data
            var bc = new BrushConverter();
          
            Border Border_TimePlace = new Border();  //tieu de
            Border_TimePlace.BorderBrush = (Brush)bc.ConvertFrom("#FF969696");
            Border_TimePlace.BorderThickness = new Thickness(0, 0, 0, 2);            

            DockPanel DP_introduce = new DockPanel();
            
            Label lb_placed = new Label(); //start -- end
            lb_placed.Content = placeSE;
            lb_placed.Style = St_Label;
            lb_placed.FontWeight = FontWeights.Bold;
            DockPanel.SetDock(lb_placed, Dock.Left);
            DP_introduce.Children.Add(lb_placed);

            Label lb_TimeS = new Label(); //start
            lb_TimeS.Content = timeS;
            lb_TimeS.Style = St_Label;
            DockPanel.SetDock(lb_TimeS, Dock.Right);
            DP_introduce.Children.Add(lb_TimeS);

            Border_TimePlace.Child = DP_introduce;
            GridBooking.Children.Add(Border_TimePlace);
            Grid.SetRow(Border_TimePlace, 0);
            Grid.SetColumn(Border_TimePlace, 0);
            Grid.SetColumnSpan(Border_TimePlace, 2);

            //<Label x:Name="lb_selected" Content="Selected seat: " Grid.Column="0" Grid.Row="1" Style="{StaticResource label_font}"/>
            Label lb_selected = new Label();
            lb_selected.Content = "Selected seat: ";
            lb_selected.Style = St_Label;
            GridBooking.Children.Add(lb_selected);
            Grid.SetColumn(lb_selected, 0);
            Grid.SetRow(lb_selected, 1);
            //<Label x:Name="lb_total" Content="Totals: " Grid.Column="0" Grid.Row="2" Style="{StaticResource label_font}"/>
            Label lb_total = new Label();
            lb_total.Content = "Totals: ";
            lb_total.Style = St_Label;
            GridBooking.Children.Add(lb_total);
            Grid.SetColumn(lb_total, 0);
            Grid.SetRow(lb_total, 2);
            //<Label x:Name="lb_PhoneNum" Content="Phone Number: " Grid.Column="0" Grid.Row="3" Style="{StaticResource label_font}"/>
            Label lb_PhoneNum = new Label();
            lb_PhoneNum.Content = "Phone Number: ";
            lb_PhoneNum.Style = St_Label;
            GridBooking.Children.Add(lb_PhoneNum);
            Grid.SetColumn(lb_PhoneNum, 0);
            Grid.SetRow(lb_PhoneNum, 3);
            //<Label x:Name="lb_FullName" Content="Your Name: " Grid.Column="0" Grid.Row="5" Style="{StaticResource label_font}"/>
            Label lb_FullName = new Label();
            lb_FullName.Content = "Your Name: ";
            lb_FullName.Style = St_Label;
            GridBooking.Children.Add(lb_FullName);
            Grid.SetColumn(lb_FullName, 0);
            Grid.SetRow(lb_FullName, 5);
            //<Label x:Name="lb_email" Content="Email: " Grid.Column="0" Grid.Row="7" Style="{StaticResource label_font}"/>
            Label lb_email = new Label();
            lb_email.Content = "Year of Birthday: ";
            lb_email.Style = St_Label;
            GridBooking.Children.Add(lb_email);
            Grid.SetColumn(lb_email, 0);
            Grid.SetRow(lb_email, 7);
            //<Label x:Name="lb_PickingPlace" Content="Picking Place: " Grid.Column="0" Grid.Row="9" Style="{StaticResource label_font}"/>
            Label lb_PickingPlace = new Label();
            lb_PickingPlace.Content = "Picking Place: ";
            lb_PickingPlace.Style = St_Label;
            GridBooking.Children.Add(lb_PickingPlace);
            Grid.SetColumn(lb_PickingPlace, 0);
            Grid.SetRow(lb_PickingPlace, 9);

            // <Label x:Name="lb_nSelected" Content="A1, A2" Grid.Column="1" Grid.Row="1" Style="{StaticResource label_font}" FontWeight="Bold"/>

            lb_nSelected.Content = "";
            lb_nSelected.Style = St_Label;
            lb_nSelected.FontWeight = FontWeights.Bold;
            lb_nSelected.HorizontalAlignment = HorizontalAlignment.Stretch;
            GridBooking.Children.Add(lb_nSelected);
            Grid.SetColumn(lb_nSelected, 1);
            Grid.SetRow(lb_nSelected, 1);
            //<Label x:Name="lb_totalPrice" Content="500,000 vnd" Grid.Column="1" Grid.Row="2" Style="{StaticResource label_font}" FontWeight="Bold"/>

            lb_totalPrice.Content = "0 vnd";
            lb_totalPrice.Style = St_Label;
            lb_totalPrice.FontWeight = FontWeights.Bold;
            GridBooking.Children.Add(lb_totalPrice);
            Grid.SetColumn(lb_totalPrice, 1);
            Grid.SetRow(lb_totalPrice, 2);

            //<TextBox x:Name="tb_PhoneN" Grid.Column="1" Grid.Row="3" Style="{StaticResource textbox_style}"/>
            TextBox tb_PhoneN = new TextBox();
            tb_PhoneN.Style = St_Textbox;
            GridBooking.Children.Add(tb_PhoneN);
            Grid.SetColumn(tb_PhoneN, 1);
            Grid.SetRow(tb_PhoneN, 3);
            //<TextBox x:Name="tb_name" Grid.Column="1" Grid.Row="5" Style="{StaticResource textbox_style}"/>
            TextBox tb_name = new TextBox();
            tb_name.Style = St_Textbox;
            GridBooking.Children.Add(tb_name);
            Grid.SetColumn(tb_name, 1);
            Grid.SetRow(tb_name, 5);
            //<TextBox x:Name="tb_email" Grid.Column="1" Grid.Row="7" Style="{StaticResource textbox_style}"/>
            TextBox tb_YearB = new TextBox();
            tb_YearB.Style = St_Textbox;
            GridBooking.Children.Add(tb_YearB);
            Grid.SetColumn(tb_YearB, 1);
            Grid.SetRow(tb_YearB, 7);
            //< TextBox x: Name = "tb_PickPlace" Grid.Column = "1" Grid.Row = "9" Style = "{StaticResource textbox_style}" />
            TextBox tb_PickPlace = new TextBox();
            tb_PickPlace.Style = St_Textbox;
            GridBooking.Children.Add(tb_PickPlace);
            Grid.SetColumn(tb_PickPlace, 1);
            Grid.SetRow(tb_PickPlace, 9);
            //<Button x:Name="bt_book" Content="Booking" Grid.Column="0" Grid.Row="12" Background="#FFFA2929" VerticalAlignment="Top" HorizontalAlignment="Right"/>
            Button bt_book = new Button();
            bt_book.Content = "Book";
            bt_book.Background = (Brush)bc.ConvertFrom("#FFFA2929");
            bt_book.VerticalAlignment = VerticalAlignment.Top;
            bt_book.HorizontalAlignment = HorizontalAlignment.Right;
            bt_book.Height = 32;
            bt_book.Width = 90;
            bt_book.FontFamily = new FontFamily("Times New Roman");
            bt_book.FontSize = 20;
            bt_book.Click += delegate
            {
                BUS.BUS_Passenger BUSPassen = new BUS.BUS_Passenger();
                BUS.BUS_Booking BUSBook = new BUS.BUS_Booking();
                BUS.BUS_Trips BUSTrips = new BUS.BUS_Trips();
                DTO.DTO_Passenger passen = new DTO_Passenger(tb_name.Text, Int32.Parse(tb_YearB.Text), tb_PhoneN.Text, DateTime.Now.ToString());
                int PassenID = new int();
                if (BUSPassen.getExistPassengerID(tb_PhoneN.Text) == 0)
                {
                    if (BUSPassen.addPassenger(passen))
                        PassenID = BUSPassen.getExistPassengerID(tb_PhoneN.Text);
                }
                else
                    PassenID = BUSPassen.getExistPassengerID(tb_PhoneN.Text);

                for (int i = 0; i < ListSeats.Count; i++)
                {
                    //int TripID = BUSTrips.getTripID(DeptTime, DeptPlace, DateTime.Now.ToString("dd-MMM-yy"));
                    DTO.DTO_Booking OneRow = new DTO_Booking(TripID, Int32.Parse(ListSeats[i]), PassenID, DateTime.Now.ToString("dd-MMM-yy"));
                    if (BUSBook.addBooking(OneRow))
                    {
                        foreach (Button seat in btSelect)
                        {
                            seat.Background = (Brush)bc.ConvertFrom("#FFFC5650");
                            seat.BorderBrush = (Brush)bc.ConvertFrom("#FFFC5650");
                        }
                        lb_nSelected.Content = "";
                        lb_totalPrice.Content = "0 vnd";
                        tb_name.Text = "";
                        tb_PhoneN.Text = "";
                        tb_PickPlace.Text = "";
                        tb_YearB.Text = "";
                    }
                }
            };
            GridBooking.Children.Add(bt_book);
            Grid.SetColumn(bt_book, 0);
            Grid.SetRow(bt_book, 12);

            return GridBooking;
        }
        
        private Border DrawBorder_ChooseSeat(int TripID, int size, double Price, Label lb_nSelected, Label lb_totalPrice, List<string> ListSeats, string DeptTime, string DeptPlace, List<Button> btSelect) //ve khung chon cho ngoi
        {
            Border Border_ChooseSeat = new Border();
            Border_ChooseSeat.Background = Brushes.White;
            Border_ChooseSeat.HorizontalAlignment = HorizontalAlignment.Stretch;
            Border_ChooseSeat.VerticalAlignment = VerticalAlignment.Stretch;

            Grid Grid_ChooseSeat = new Grid();

            //Create column
            ColumnDefinition GridCol1 = new ColumnDefinition();
            GridCol1.Width = new GridLength(20, GridUnitType.Pixel);
            ColumnDefinition GridCol2 = new ColumnDefinition();
            GridCol2.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition GridCol3 = new ColumnDefinition();
            GridCol3.Width = new GridLength(10);
            ColumnDefinition GridCol4 = new ColumnDefinition();
            ColumnDefinition GridCol5 = new ColumnDefinition();
            GridCol5.Width = new GridLength(10);
            ColumnDefinition GridCol6 = new ColumnDefinition();
            ColumnDefinition GridCol7 = new ColumnDefinition();
            GridCol7.Width = new GridLength(10);
            ColumnDefinition GridCol8 = new ColumnDefinition();
            ColumnDefinition GridCol9 = new ColumnDefinition();
            GridCol9.Width = new GridLength(10);
            ColumnDefinition GridCol10 = new ColumnDefinition();
            ColumnDefinition GridCol11 = new ColumnDefinition();
            GridCol11.Width = new GridLength(10);
            ColumnDefinition GridCol12 = new ColumnDefinition();
            ColumnDefinition GridCol13 = new ColumnDefinition();
            GridCol13.Width = new GridLength(10);
            ColumnDefinition GridCol14 = new ColumnDefinition();
            ColumnDefinition GridCol15 = new ColumnDefinition();
            GridCol15.Width = new GridLength(10);
            ColumnDefinition GridCol16 = new ColumnDefinition();
            ColumnDefinition GridCol17 = new ColumnDefinition();
            GridCol17.Width = new GridLength(20, GridUnitType.Pixel);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol1);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol2);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol3);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol4);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol5);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol6);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol7);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol8);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol9);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol10);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol11);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol12);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol13);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol14);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol15);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol16);
            Grid_ChooseSeat.ColumnDefinitions.Add(GridCol17);

            //create row
            RowDefinition GridRow1 = new RowDefinition();
            GridRow1.Height = new GridLength(15);
            RowDefinition GridRow2 = new RowDefinition();
            RowDefinition GridRow3 = new RowDefinition();
            GridRow3.Height = new GridLength(10);
            RowDefinition GridRow4 = new RowDefinition();
            RowDefinition GridRow5 = new RowDefinition();
            GridRow5.Height = new GridLength(30);
            RowDefinition GridRow6 = new RowDefinition();
            RowDefinition GridRow7 = new RowDefinition();
            GridRow7.Height = new GridLength(10);
            RowDefinition GridRow8 = new RowDefinition();
            RowDefinition GridRow9 = new RowDefinition();
            GridRow9.Height = new GridLength(15);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow1);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow2);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow3);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow4);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow5);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow6);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow7);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow8);
            Grid_ChooseSeat.RowDefinitions.Add(GridRow9);

            /*<Button Grid.Column="1" Content="01" Grid.Row="1" Background="#FFFC5650" BorderBrush="#FFFC5650" />
              */

            Border_ChooseSeat.Child = Grid_ChooseSeat;

            //create seats
            BUS.BUS_Trips BUSTrips = new BUS.BUS_Trips();
            BUS.BUS_Booking BUSBook = new BUS.BUS_Booking();
            //int TripID = BUSTrips.getTripID(DeptTime, DeptPlace, DateTime.Now.ToString("dd-MMM-yy"));
            List<int> lsSeat = BUSBook.isSelectedSeat(TripID);
            double TotalPrice = 0;
            int runCol = 13;
            int runRow = 1;
            int count = 0;
            for (int i = 1; i <= size; i++)
            {
                

                Button Seat1 = new Button();
                var bc = new BrushConverter();
                
                Seat1.HorizontalAlignment = HorizontalAlignment.Stretch;
                Seat1.VerticalAlignment = VerticalAlignment.Stretch;
                
                if (i <= 9)
                    Seat1.Content = "0" + i;
                else
                    Seat1.Content = i;

                if (i == 1 || i == 2)
                {
                    Grid.SetColumn(Seat1, 15);
                    if (i == 1)
                        Grid.SetRow(Seat1, 5);
                    else
                        Grid.SetRow(Seat1, 7);
                }
                else if (i == 7 || i == 8)
                {
                    Grid.SetColumn(Seat1, 11);
                    if (i == 7)
                        Grid.SetRow(Seat1, 1);
                    else
                        Grid.SetRow(Seat1, 3);
                    runCol -= 1;
                }
                else if (i == 27)
                {
                    Grid.SetColumn(Seat1, 1);
                    Grid.SetRow(Seat1, 4);
                }
                else if (i == 28)
                {
                    Grid.SetColumn(Seat1, 1);
                    Grid.SetRow(Seat1, 5);
                }
                else if (i == 29)
                {
                    Grid.SetColumn(Seat1, 1);
                    Grid.SetRow(Seat1, 7);
                }
                else
                {
                    if (count == 4)
                    {
                        count = 0;
                        runCol -= 2;
                    }
                    Grid.SetColumn(Seat1, runCol);
                    Grid.SetRow(Seat1, runRow);
                    count++;
                    if (runRow == 7)
                        runRow = 1;
                    else
                        runRow += 2;
                }
                
                if (lsSeat.Contains(Int32.Parse(Seat1.Content.ToString())))
                {
                    Seat1.Background = (Brush)bc.ConvertFrom("#FFFC5650");
                    Seat1.BorderBrush = (Brush)bc.ConvertFrom("#FFFC5650");
                }
                else
                {
                    Seat1.Background = Brushes.White;
                    Seat1.BorderBrush = Brushes.White;
                    Seat1.Foreground = Brushes.Black;
                }

                int numberOfClick = 0;
                Seat1.Click += delegate
                {
                    if (!lsSeat.Contains(Int32.Parse(Seat1.Content.ToString())))
                    {
                        numberOfClick++;

                        if (numberOfClick % 2 != 0)
                        {
                            Seat1.Background = (Brush)bc.ConvertFrom("#FFADADAD");
                            Seat1.BorderBrush = (Brush)bc.ConvertFrom("#FFADADAD");

                            if (ListSeats.Count >= 6)
                            {
                                MessageBox.Show("You only buy up to 6 tickets for 1 transaction.", "Error");
                                Seat1.Background = Brushes.White;
                                Seat1.BorderBrush = Brushes.White;
                                numberOfClick--;
                            }
                            else
                            {
                                ListSeats.Add(Seat1.Content.ToString());
                                btSelect.Add(Seat1);
                            }
                            TotalPrice += Price;
                            lb_totalPrice.Content = TotalPrice + " vnd";
                            string LSeat = ListSeats[0];
                            for (int j = 1; j < ListSeats.Count; j++)
                                LSeat += ", " + ListSeats[j];
                            lb_nSelected.Content = LSeat;                           
                        }
                        else if (numberOfClick % 2 == 0)
                        {
                            Seat1.Background = Brushes.White;
                            Seat1.BorderBrush = Brushes.White;

                            ListSeats.Remove(Seat1.Content.ToString());
                            btSelect.Remove(Seat1);
                            TotalPrice -= Price;
                            lb_totalPrice.Content = TotalPrice + " vnd";
                            string LSeat = "";
                            for (int j = 0; j < ListSeats.Count; j++)
                                if (j == 0)
                                    LSeat = ListSeats[0];
                                else
                                    LSeat += ", " + ListSeats[j];
                            lb_nSelected.Content = LSeat;
                            
                        }
                    }

                };
                Grid_ChooseSeat.Children.Add(Seat1);
            }
            return Border_ChooseSeat;
        }

        private Border DrawBorder_ChooseOrder(int TripID, int size, string placeSE, string timeS, double Price, string DeptTime, string DeptPlace) //ve border mau xam
        {
            Border Border_ChooseOrder = new Border();
            Border_ChooseOrder.Height = 400;
            Border_ChooseOrder.Background = Brushes.Gainsboro;
            Border_ChooseOrder.BorderBrush = Brushes.White;
            Border_ChooseOrder.Visibility = Visibility.Collapsed;
            Border_ChooseOrder.BorderThickness = new Thickness(0,0,0,2);

            Grid Grid_ChooseOrder = new Grid();

            //create column
            ColumnDefinition GridCol1 = new ColumnDefinition();
            GridCol1.Width = new GridLength(1.8,GridUnitType.Star);
            ColumnDefinition GridCol2 = new ColumnDefinition();
            GridCol2.Width = new GridLength(1, GridUnitType.Star);
            ColumnDefinition GridCol3 = new ColumnDefinition();
            GridCol3.Width = new GridLength(30);
            Grid_ChooseOrder.ColumnDefinitions.Add(GridCol1);
            Grid_ChooseOrder.ColumnDefinitions.Add(GridCol2);
            Grid_ChooseOrder.ColumnDefinitions.Add(GridCol3);

            Border_ChooseOrder.Child = Grid_ChooseOrder;

            //<Grid Grid.Column="0">
            Grid Grid0 = new Grid();
            //create row
            RowDefinition GridRow01 = new RowDefinition();
            GridRow01.Height = new GridLength(2, GridUnitType.Star);
            RowDefinition GridRow02 = new RowDefinition();
            GridRow02.Height = new GridLength(1, GridUnitType.Star);
            Grid0.RowDefinitions.Add(GridRow01);
            Grid0.RowDefinitions.Add(GridRow02);

            Grid_ChooseOrder.Children.Add(Grid0);
            Grid.SetColumn(Grid0, 0);

            //<Grid Grid.Row="0">
            Grid Grid00 = new Grid();
            
            //create row
            RowDefinition GridRow001 = new RowDefinition();
            GridRow001.Height = new GridLength(30);
            RowDefinition GridRow002 = new RowDefinition();
            GridRow002.Height = new GridLength(1, GridUnitType.Star);
            Grid00.RowDefinitions.Add(GridRow001);
            Grid00.RowDefinitions.Add(GridRow002);
            //create column
            ColumnDefinition GridCol001 = new ColumnDefinition();
            GridCol001.Width = new GridLength(30);
            ColumnDefinition GridCol002 = new ColumnDefinition();
            ColumnDefinition GridCol003 = new ColumnDefinition();
            GridCol003.Width = new GridLength(30);
            Grid00.ColumnDefinitions.Add(GridCol001);
            Grid00.ColumnDefinitions.Add(GridCol002);
            Grid00.ColumnDefinitions.Add(GridCol003);

            Grid0.Children.Add(Grid00);
            Grid.SetRow(Grid00, 0);

            Label lb_nSelected = new Label();
            Label lb_totalPrice = new Label();
            List<string> ListSeats = new List<string>();
            List<Button> btSelect = new List<Button>();

            //add border_chooseSeat
            Border Border_ChooseSeat = DrawBorder_ChooseSeat(TripID, size, Price, lb_nSelected, lb_totalPrice, ListSeats, DeptTime, DeptPlace, btSelect);
            Grid00.Children.Add(Border_ChooseSeat);
            Grid.SetRow(Border_ChooseSeat, 1);
            Grid.SetColumn(Border_ChooseSeat, 1);

            //add information
            Grid GridInfor = DrawBooking(TripID, placeSE, timeS, lb_nSelected, lb_totalPrice, ListSeats, DeptTime, DeptPlace, btSelect);
            Grid_ChooseOrder.Children.Add(GridInfor);
            Grid.SetColumn(GridInfor, 1);

            //add GridNote
            Grid GridNote = DrawNote();
            Grid0.Children.Add(GridNote);
            Grid.SetRow(GridNote, 1);

           

            return Border_ChooseOrder;
        }

        private Border DrawTrips_Infor(int index, string TimeSE, double DuringTime, int nOnlineSeats, string BusType, double Price) // thanh mau trang
        {
            //< Border BorderThickness = "0,0,0,2" Height = "120" BorderBrush = "Gainsboro" />
            Border Border_Trip = new Border();
            Border_Trip.BorderThickness = new Thickness(0, 2, 0, 0);
            Border_Trip.Height = 120;
            Border_Trip.BorderBrush = Brushes.Gainsboro;

            //Grid
            Grid Grid_trips = new Grid();

            //create column
            ColumnDefinition GridCol0 = new ColumnDefinition();
            GridCol0.Width = new GridLength(40);
            ColumnDefinition GridCol1 = new ColumnDefinition();
            ColumnDefinition GridCol2 = new ColumnDefinition();
            GridCol2.Width = new GridLength(1.8, GridUnitType.Star);
            ColumnDefinition GridCol3 = new ColumnDefinition();
            GridCol3.Width = new GridLength(40);
            ColumnDefinition GridCol4 = new ColumnDefinition();
            GridCol4.Width = new GridLength(1.3, GridUnitType.Star);
            ColumnDefinition GridCol5 = new ColumnDefinition();
            GridCol5.Width = new GridLength(1.2, GridUnitType.Star);
            ColumnDefinition GridCol6 = new ColumnDefinition();
            Grid_trips.ColumnDefinitions.Add(GridCol0);
            Grid_trips.ColumnDefinitions.Add(GridCol1);
            Grid_trips.ColumnDefinitions.Add(GridCol2);
            Grid_trips.ColumnDefinitions.Add(GridCol3);
            Grid_trips.ColumnDefinitions.Add(GridCol4);
            Grid_trips.ColumnDefinitions.Add(GridCol5);
            Grid_trips.ColumnDefinitions.Add(GridCol6);

            //create row
            RowDefinition Gridrow0 = new RowDefinition();
            RowDefinition Gridrow1 = new RowDefinition();
            RowDefinition Gridrow2 = new RowDefinition();
            Gridrow2.Height = new GridLength(0.5, GridUnitType.Star);
            Grid_trips.RowDefinitions.Add(Gridrow0);
            Grid_trips.RowDefinitions.Add(Gridrow1);
            Grid_trips.RowDefinitions.Add(Gridrow2);

            Border_Trip.Child = Grid_trips;

            //add data

            //<materialDesign:PackIcon Kind="AccessTime" Grid.Column="0" Grid.Row="0" Grid.RowSpan="2" Width="40" Height="40" HorizontalAlignment="Center" VerticalAlignment="Center"/>
            Image AccessTime = new Image();
            AccessTime.Source = new BitmapImage(new Uri(@"/GUI;component/image/outline_access_time.png", UriKind.Relative));
            AccessTime.Height = 40;
            AccessTime.Width = 40;
            AccessTime.HorizontalAlignment = HorizontalAlignment.Center;
            AccessTime.VerticalAlignment = VerticalAlignment.Center;
            Grid_trips.Children.Add(AccessTime);
            Grid.SetColumn(AccessTime, 0);
            Grid.SetRow(AccessTime, 0);
            Grid.SetRowSpan(AccessTime, 2);

            //<materialDesign:PackIcon Kind="AirlineSeatReclineNormal" Grid.Column="3" Grid.Row="0" Grid.RowSpan="2" Width="40" Height="40" HorizontalAlignment="Center" VerticalAlignment="Center"/>
            Image AirlineSeat = new Image();
            AirlineSeat.Source = new BitmapImage(new Uri(@"/GUI;component/image/outline_airline_seat_recline_normal_black_36dp.png", UriKind.Relative));
            AirlineSeat.Height = 40;
            AirlineSeat.Width = 40;
            AirlineSeat.HorizontalAlignment = HorizontalAlignment.Center;
            AirlineSeat.VerticalAlignment = VerticalAlignment.Center;
            Grid_trips.Children.Add(AirlineSeat);
            Grid.SetColumn(AirlineSeat, 3);
            Grid.SetRow(AirlineSeat, 0);
            Grid.SetRowSpan(AirlineSeat, 2);

            var bc = new BrushConverter();
            //<Label x:Name="lb_PointTime" Content="7:00 - 10:00" Grid.Column="1" Grid.Row="0" FontFamily="Times New Roman" FontSize="20" FontWeight="Bold" HorizontalAlignment="Left" VerticalAlignment="Bottom"/>
            Label lb_PointTime = new Label();
            lb_PointTime.Content = TimeSE;
            lb_PointTime.FontFamily = new FontFamily("Times New Roman");
            lb_PointTime.FontSize = 20;
            lb_PointTime.FontWeight = FontWeights.Bold;
            lb_PointTime.HorizontalAlignment = HorizontalAlignment.Left;
            lb_PointTime.VerticalAlignment = VerticalAlignment.Bottom;
            Grid_trips.Children.Add(lb_PointTime);
            Grid.SetColumn(lb_PointTime, 1);
            Grid.SetRow(lb_PointTime, 0);
            //<Label x:Name="lb_time" Content="Time: 3 hours" Grid.Column="1" Grid.Row="1" FontFamily="Times New Roman" FontSize="20" HorizontalAlignment="Left" VerticalAlignment="Top" Foreground="#DD6B6B6B"/>
            Label lb_time = new Label();
            lb_time.Content = "Time: " + DuringTime + " hours";
            lb_time.FontFamily = new FontFamily("Times New Roman");
            lb_time.FontSize = 20;
            lb_time.Foreground = (Brush)bc.ConvertFrom("#DD6B6B6B");
            lb_time.HorizontalAlignment = HorizontalAlignment.Left;
            lb_time.VerticalAlignment = VerticalAlignment.Top;
            Grid_trips.Children.Add(lb_time);
            Grid.SetColumn(lb_time, 1);
            Grid.SetRow(lb_time, 1);
            //<Label x:Name="lb_nSeat" Content="17 Online seats" Grid.Column="4" Grid.Row="0" FontFamily="Times New Roman" FontSize="20" FontWeight="Bold" HorizontalAlignment="Left" VerticalAlignment="Bottom"/>
            Label lb_nSeat = new Label();
            lb_nSeat.Content = nOnlineSeats + " Online Seats";
            lb_nSeat.FontFamily = new FontFamily("Times New Roman");
            lb_nSeat.FontSize = 20;
            lb_nSeat.FontWeight = FontWeights.Bold;
            lb_nSeat.HorizontalAlignment = HorizontalAlignment.Left;
            lb_nSeat.VerticalAlignment = VerticalAlignment.Bottom;
            Grid_trips.Children.Add(lb_nSeat);
            Grid.SetColumn(lb_nSeat, 4);
            Grid.SetRow(lb_nSeat, 0);
            //<Label x:Name="lb_TypeBus" Content="(Limosine)" Grid.Column="4" Grid.Row="1" FontFamily="Times New Roman" FontSize="20" HorizontalAlignment="Left" VerticalAlignment="Top" Foreground="#DD6B6B6B"/>
            Label lb_TypeBus = new Label();
            lb_TypeBus.Content = "(" + BusType + ")";
            lb_TypeBus.FontFamily = new FontFamily("Times New Roman");
            lb_TypeBus.FontSize = 20;
            lb_TypeBus.Foreground = (Brush)bc.ConvertFrom("#DD6B6B6B");
            lb_TypeBus.HorizontalAlignment = HorizontalAlignment.Left;
            lb_TypeBus.VerticalAlignment = VerticalAlignment.Top;
            Grid_trips.Children.Add(lb_TypeBus);
            Grid.SetColumn(lb_TypeBus, 4);
            Grid.SetRow(lb_TypeBus, 1);
            //<Label x:Name="lb_Price" Content="273,000 vnd" Grid.Column="5" Grid.Row="0" Grid.RowSpan="2" Foreground="#DDF90303" FontFamily="Times New Roman" FontSize="24" FontWeight="Bold" VerticalAlignment="Center"/>
            Label lb_Price = new Label();
            lb_Price.Content = Price + " vnd";
            lb_Price.FontFamily = new FontFamily("Times New Roman");
            lb_Price.FontSize = 24;
            lb_Price.FontWeight = FontWeights.Bold;
            lb_Price.Foreground = (Brush)bc.ConvertFrom("#DDF90303");
            lb_Price.VerticalAlignment = VerticalAlignment.Center;
            Grid_trips.Children.Add(lb_Price);
            Grid.SetColumn(lb_Price, 5);
            Grid.SetRow(lb_Price, 0);
            Grid.SetRowSpan(lb_Price, 2);

            //<Button x:Name="bt_ordering" Content="Booking" BorderThickness="1.5" Grid.Column="6" Grid.Row="0" Grid.RowSpan="2" VerticalAlignment="Center" Background="#FFFA2929" FontFamily="Times New Roman" FontSize="23" Click="Bt_ordering_Click" BorderBrush="White"/>
            Button bt_ordering = new Button();
            bt_ordering.Content = "Booking";
            bt_ordering.BorderThickness = new Thickness(1.5);
            bt_ordering.VerticalAlignment = VerticalAlignment.Center;
            bt_ordering.Background = (Brush)bc.ConvertFrom("#FFFA2929");
            bt_ordering.FontFamily = new FontFamily("Times New Roman");
            bt_ordering.FontSize = 23;
            bt_ordering.BorderBrush = Brushes.White;
            bt_ordering.Click += delegate
            {
                if ((string)bt_ordering.Content == "Booking")
                {
                    ArrChooseOrder[index].Visibility = Visibility.Visible;
                    bt_ordering.Content = "Hidden";
                }
                else if ((string)bt_ordering.Content == "Hidden")
                {
                    ArrChooseOrder[index].Visibility = Visibility.Collapsed;
                    bt_ordering.Content = "Booking";
                }
            };
            Grid_trips.Children.Add(bt_ordering);
            Grid.SetColumn(bt_ordering, 6);
            Grid.SetRow(bt_ordering, 0);
            Grid.SetRowSpan(bt_ordering, 2);

            return Border_Trip;
        }

        
        private void DrawTrips(List<DTO.TripsInfor> ListTripsInfor)
        {
            if (ListTripsInfor.Count == 0)
            {
                //trips_order.Children.Add(Border_ErrorMsg);
                Border_ErrorMsg.Visibility = Visibility.Visible;
            }               
            else
            {
                Border_ErrorMsg.Visibility = Visibility.Collapsed;
                for (int i = 0; i < ListTripsInfor.Count; i++)
                {
                    string TimeSE = "Departure time: " + ListTripsInfor[i].getDepTime();
                    Arrtrips[i] = DrawTrips_Infor(i, TimeSE, 3, 29, "Huydai 30 seats", (double)ListTripsInfor[i].getPrice());
                    trips_order.Children.Add(Arrtrips[i]);
                    ArrChooseOrder[i] = DrawBorder_ChooseOrder(ListTripsInfor[i].getTripID(), 29, lb_departPlace.Text, "Date: " + ListTripsInfor[i].getDate(), (double)ListTripsInfor[i].getPrice(), ListTripsInfor[i].getDepTime(), ListTripsInfor[i].getDepPlace());
                    trips_order.Children.Add(ArrChooseOrder[i]);
                }
            }
            
            
        }        

        private void Bt_search_MouseEnter(object sender, MouseEventArgs e)
        {
            
            bt_Icon_search.Visibility = Visibility.Visible;
        }

       
        private void Bt_Icon_search_MouseLeave(object sender, MouseEventArgs e)
        {
            bt_Icon_search.Visibility = Visibility.Hidden;
        }

        private void Bt_Icon_search_Click(object sender, RoutedEventArgs e)
        {
            trips_order.Children.Clear();

            ListTripsInfor = BUS_trip.getListTripsInfor(dp_date.Text, this.DeparturePlace);
            DrawTrips(ListTripsInfor);

        }
    }
}
