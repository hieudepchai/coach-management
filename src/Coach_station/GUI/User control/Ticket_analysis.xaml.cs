﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI.User_control
{
    /// <summary>
    /// Interaction logic for Ticket_analysis.xaml
    /// </summary>
    public partial class Ticket_analysis : UserControl
    {
        private BUS.Ticket_analysis Ta = new BUS.Ticket_analysis();
        private List<DTO.Tickets_analysis> listnTickets;
        private BUS.Finance_analysis Fa = new BUS.Finance_analysis();
        List<string> ListTimes;
        
        public Ticket_analysis()
        {
            InitializeComponent();
        }
                
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DateTime now = DateTime.Now;
            dp_StartDate.Text = now.ToString();
            dp_EndDate.Text = now.ToString();
            Bt_Statistic_Click(sender, e);
        }

        private void Bt_Statistic_Click(object sender, RoutedEventArgs e)
        {
            this.listnTickets = Ta.getnTicket(dp_StartDate.Text, dp_EndDate.Text);
            ListTimes = Fa.getListTimes();
            SetPoint(this.listnTickets, this.ListTimes);

        }

        private void Bt_Statistic_MouseEnter(object sender, MouseEventArgs e)
        {
            bt_Statistic.Background = Brushes.Gray;
        }

        private void Bt_Statistic_MouseLeave(object sender, MouseEventArgs e)
        {
            bt_Statistic.Background = Brushes.White;
        }

        private void SetPoint(List<DTO.Tickets_analysis> listnTickets, List<string> ListTimes)
        {
            List<string> ListTimeH = new List<string>();
            for (int i = 0; i < listnTickets.Count; i++)
                ListTimeH.Add(listnTickets[i].GetDepTime());
            for (int i = 0; i < ListTimes.Count; i++)
            {
                if (!ListTimeH.Contains(ListTimes[i]))
                {
                    DTO.Tickets_analysis onerow = new DTO.Tickets_analysis(0, ListTimes[i]);
                    listnTickets.Add(onerow);
                }
            }
            col1.Argument = listnTickets[0].GetDepTime();
            col1.Value = listnTickets[0].GetnTickets();

            col2.Argument = listnTickets[1].GetDepTime();
            col2.Value = listnTickets[1].GetnTickets();

            col3.Argument = listnTickets[2].GetDepTime();
            col3.Value = listnTickets[2].GetnTickets();

            col4.Argument = listnTickets[3].GetDepTime();
            col4.Value = listnTickets[3].GetnTickets();

            col5.Argument = listnTickets[4].GetDepTime();
            col5.Value = listnTickets[4].GetnTickets();

            col6.Argument = listnTickets[5].GetDepTime();
            col6.Value = listnTickets[5].GetnTickets();

            col7.Argument = listnTickets[6].GetDepTime();
            col7.Value = listnTickets[6].GetnTickets();

            col8.Argument = listnTickets[7].GetDepTime();
            col8.Value = listnTickets[7].GetnTickets();

            col9.Argument = listnTickets[8].GetDepTime();
            col9.Value = listnTickets[8].GetnTickets();

            col10.Argument = listnTickets[9].GetDepTime();
            col10.Value = listnTickets[9].GetnTickets();

            col11.Argument = listnTickets[10].GetDepTime();
            col11.Value = listnTickets[10].GetnTickets();

            col12.Argument = listnTickets[11].GetDepTime();
            col12.Value = listnTickets[11].GetnTickets();

            col13.Argument = listnTickets[12].GetDepTime();
            col13.Value = listnTickets[12].GetnTickets();

            col14.Argument = listnTickets[13].GetDepTime();
            col14.Value = listnTickets[13].GetnTickets();

            col15.Argument = listnTickets[14].GetDepTime();
            col15.Value = listnTickets[14].GetnTickets();

            col16.Argument = listnTickets[15].GetDepTime();
            col16.Value = listnTickets[15].GetnTickets();

            col17.Argument = listnTickets[16].GetDepTime();
            col17.Value = listnTickets[16].GetnTickets();

            col18.Argument = listnTickets[17].GetDepTime();
            col18.Value = listnTickets[17].GetnTickets();

            col19.Argument = listnTickets[18].GetDepTime();
            col19.Value = listnTickets[18].GetnTickets();

            col20.Argument = listnTickets[19].GetDepTime();
            col20.Value = listnTickets[19].GetnTickets();

            col21.Argument = listnTickets[20].GetDepTime();
            col21.Value = listnTickets[20].GetnTickets();

            col22.Argument = listnTickets[21].GetDepTime();
            col22.Value = listnTickets[21].GetnTickets();

            col23.Argument = listnTickets[22].GetDepTime();
            col23.Value = listnTickets[22].GetnTickets();

            col24.Argument = listnTickets[23].GetDepTime();
            col24.Value = listnTickets[23].GetnTickets();

            col25.Argument = listnTickets[24].GetDepTime();
            col25.Value = listnTickets[24].GetnTickets();

            col26.Argument = listnTickets[25].GetDepTime();
            col26.Value = listnTickets[25].GetnTickets();

            col27.Argument = listnTickets[26].GetDepTime();
            col27.Value = listnTickets[26].GetnTickets();

            col28.Argument = listnTickets[27].GetDepTime();
            col28.Value = listnTickets[27].GetnTickets();

            col29.Argument = listnTickets[28].GetDepTime();
            col29.Value = listnTickets[28].GetnTickets();

            col30.Argument = listnTickets[29].GetDepTime();
            col30.Value = listnTickets[29].GetnTickets();

            col31.Argument = listnTickets[30].GetDepTime();
            col31.Value = listnTickets[30].GetnTickets();

            col32.Argument = listnTickets[31].GetDepTime();
            col32.Value = listnTickets[31].GetnTickets();

        }
    }
}
