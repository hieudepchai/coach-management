﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using BUS;
using DTO;

namespace GUI
{
    /// <summary>
    /// Interaction logic for UC_Manage_Customer.xaml
    /// </summary>
    public partial class UC_Manage_Customer : UserControl
    {
        private MainWindow window;
        BUS_Passenger BUSpassenger = new BUS_Passenger();
        int function = 0;
        public UC_Manage_Customer()
        {
            InitializeComponent();
            PassengerGrid.ItemsSource = BUSpassenger.getPassenger().DefaultView;
            btn_save.IsEnabled = false;
        }
        public void setWindow(MainWindow window)
        {
            this.window = window;
        }
        public int isNotEmpty()
        {
            if (tbx_fullname.Text == "") return 0;
            if (tbx_birthyear.Text == "") return 0;
            if (tbx_phonenum.Text == "") return 0;
            if (tbx_date.Text == "") return 0;
            return 1;
        }
        public void enableButton(Boolean t)
        {
            btn_add.IsEnabled = t;
            btn_delete.IsEnabled = t;
            btn_edit.IsEnabled = t;
            btn_save.IsEnabled = t;
        }
        public void enableTextBox(Boolean t)
        {
            tbx_ID.IsEnabled = t;
            tbx_fullname.IsEnabled = t;
            tbx_birthyear.IsEnabled = t;
            tbx_date.IsEnabled = t;
        }

        public void EndFunction()
        {
            clearTextBox();
            enableButton(true);
            enableTextBox(true);
            btn_save.IsEnabled = false;
            PassengerGrid.ItemsSource = BUSpassenger.getPassenger().DefaultView;
            function = 0;

        }
        public void clearTextBox()
        {
            tbx_ID.Text = "";
            tbx_birthyear.Text = "";
            tbx_phonenum.Text = "";
            tbx_date.Text = "";
            tbx_fullname.Text = "";
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            switch (function)
            {
                case 1:
                    {
                        if (isNotEmpty() == 1)
                        {

                            DTO_Passenger dtpass = new DTO_Passenger(tbx_fullname.Text, Int32.Parse(tbx_birthyear.Text), tbx_phonenum.Text, tbx_date.Text);

                            if (BUSpassenger.addPassenger(dtpass) == false) MessageBox.Show("Failed to insert!!!");
                            else
                            {
                                MessageBox.Show("Done!!!");
                            }
                            EndFunction();
                        }
                        else
                        {
                            MessageBox.Show("No empty content allowed!!!");
                        }
                        break;
                    }
                case 2:
                    {

                        {
                            if (tbx_ID.Text != "")
                            {

                                if (BUSpassenger.deletePassenger(Int32.Parse(tbx_ID.Text)) == 0) MessageBox.Show("Failed to delete!!!");
                                else MessageBox.Show("Done!!!");
                            }
                            else
                            {
                                MessageBox.Show("Phone Number must have value!!!");
                                EndFunction();
                            }
                            break;
                        }
                    }
                case 3:
                    {
                        DTO_Passenger dtpass = new DTO_Passenger(Int32.Parse(tbx_ID.Text), tbx_fullname.Text, Int32.Parse(tbx_birthyear.Text), tbx_phonenum.Text, tbx_date.Text);
                        if (BUSpassenger.editPassenger(dtpass) == 0) MessageBox.Show("Fail to update!!!");
                        else
                        {
                            MessageBox.Show("Done!!!");
                            EndFunction();
                        }
                        break;
                    }

            }
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            clearTextBox();
            function = 1;
            btn_save.IsEnabled = true;
            tbx_ID.IsEnabled = false;
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            tbx_phonenum.IsEnabled = true;
            function = 2;
            btn_save.IsEnabled = true;
            tbx_ID.IsEnabled = false;
        }

        private void btn_edit_Click(object sender, RoutedEventArgs e)
        {
            btn_save.IsEnabled = true;
            tbx_ID.IsEnabled = false;
            function = 3;
        }

        private void btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            enableTextBox(true);
            enableButton(true);
            clearTextBox();
            btn_save.IsEnabled = false;
            PassengerGrid.ItemsSource = BUSpassenger.getPassenger().DefaultView;
        }

        private void PassengerGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            clearTextBox();
            DataGrid dg = (DataGrid)sender;
            DataRowView row_selected = dg.SelectedItem as DataRowView;
            if (row_selected != null)
            {
                tbx_ID.Text = row_selected["PassengerID"].ToString();
                tbx_fullname.Text = row_selected["Fullname"].ToString();
                tbx_birthyear.Text = row_selected["BirthYear"].ToString();
                tbx_phonenum.Text = row_selected["PhoneNumber"].ToString();
                tbx_date.Text = row_selected["CreatedDate"].ToString();
            }
        }

        internal void ShowDialog()
        {
            throw new NotImplementedException();
        }
    }
}
