﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BUS;
using DTO;

namespace GUI.User_control
{
    /// <summary>
    /// Interaction logic for UC_SeatMap.xaml
    /// </summary>
    public partial class UC_SeatMap : UserControl
    {
        BUS_Booking bus_booking = new BUS_Booking();
        List<int> BookedSeat = new List<int>();
        List<Tuple<DTO_Passenger, int>> psgseatList = new List<Tuple<DTO_Passenger, int>>();
        Brush prevBrush;
        string backgroundf;
        private GUI_StaffMenu staffmenu;
        public string DepartureTime;
        public int DeparturePlace;
        public string date;
        public int TripID;
        public UC_SeatMap()
        {
            InitializeComponent();
        }
        private void UC_SeatMap_Load(object sender, RoutedEventArgs e)
        {
            dpDate.Text = date;
            cbbDepartPlace.SelectedIndex = DeparturePlace;
            BookedSeat = bus_booking.isSelectedSeat(TripID);
            psgseatList = bus_booking.getPsgandSeatList(TripID);
            FillBookedSeat();
        }

        public void FillBookedSeat()
        {
            foreach (int no in BookedSeat)
            {
                string cellname = String.Concat("seat",no.ToString());
                Border bd = (Border)SeatGrid.FindName(cellname);
                bd.Background = (Brush)new BrushConverter().ConvertFrom("#96f74c7f");
            }
        }
        public void setStaffMenu(GUI_StaffMenu new_staffmenu)
        {
            this.staffmenu = new_staffmenu;
        }
        private void cbbDepartTime_Load(object sender, RoutedEventArgs e)
        {
            ComboBox cbb = e.Source as ComboBox;
            List<String> DepartTimeList = new List<String>();
            int i;
            for (i = 4; i <= 19; i++)
            {
                DepartTimeList.Add(String.Concat(i.ToString(), ":00"));
                DepartTimeList.Add(String.Concat(i.ToString(), ":30"));
            }
            cbb.ItemsSource = DepartTimeList;
            cbbDepartTime.Text = DepartureTime;

        }

        private void cell_mouse_enter(object sender, MouseEventArgs e)
        {
            Border thisBorder = e.Source as Border;
            prevBrush = thisBorder.Background;
            thisBorder.Background = (Brush)new BrushConverter().ConvertFrom("#26111111"); 
        }

        private void cell_mouse_leave(object sender, MouseEventArgs e)
        {
            Border thisBorder = e.Source as Border;
            //thisBorder.Background = (Brush)new BrushConverter().ConvertFrom("#FFF");
            thisBorder.Background = prevBrush;
        }

        private void popup_seatinfo(object sender, MouseButtonEventArgs e)
        {
            var thisBorder = e.Source as Border;
            var new_popup_seatinfo = new GUI_popup_psginfo();
            new_popup_seatinfo.str_seatno = thisBorder.Name.Substring(4);  
             foreach (Tuple<DTO_Passenger, int> item in psgseatList)
                {
                    if (item.Item2 == Int32.Parse(new_popup_seatinfo.str_seatno))
                    {
                        new_popup_seatinfo.psg = item.Item1;
                    break;

                    }
                     new_popup_seatinfo.psg = null;
                }
            new_popup_seatinfo.ShowDialog();
        
        }

       
    }
}
