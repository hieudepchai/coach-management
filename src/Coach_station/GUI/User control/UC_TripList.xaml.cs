﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DTO;
using BUS;
namespace GUI.User_control
{
    /// <summary>
    /// Interaction logic for UC_TripList.xaml
    /// </summary>
    public partial class UC_TripList : UserControl
    {
        BUS_Trip bus_trip = new BUS_Trip();
        private GUI_StaffMenu staffmenu;
        public string date;
        public int DepartPlace;
        public string DepartTime;
        public List<Tuple<string,string,int, DTO_Trip, int>> triplist = new List<Tuple<string,string,int, DTO_Trip, int>>();
        public List<TextBlock> tbSeatList = new List<TextBlock>();
        public List<TextBlock> tbDriverList = new List<TextBlock>();
        public List<TextBlock> tbPlateList = new List<TextBlock>();
        public UC_TripList()
        {
            InitializeComponent();
            addDepartTimeList();
            RenderFullTrip();
        }
        public void setStaffMenu(GUI_StaffMenu new_staffmenu)
        {
            this.staffmenu = new_staffmenu;
        }
        private void tripgridPreviewMouseDown(object sender, RoutedEventArgs e)
        {
            var cell = e.Source as FrameworkElement;
            String cellname = cell.Name;
            int r = Int32.Parse(cellname[4].ToString());
            int c = Int32.Parse(cellname[5].ToString());
            int hour, min;
            int index = r * 6 + c;
            String time = DepartTimeList[index];
            if (time.Length == 5)
            {
                hour = Int32.Parse(time.Substring(0, 2));
                min = Int32.Parse(time.Substring(3,2));
                int z = 0;
            }
            else
            {
                hour = Int32.Parse(time.Substring(0, 1));
                min = Int32.Parse(time.Substring(2,2));
            }
            UC_SeatMap new_uc_seatmap = new UC_SeatMap();
            new_uc_seatmap.DeparturePlace = cbbDepartPlace.SelectedIndex;
            new_uc_seatmap.date = dpDate.Text;
            new_uc_seatmap.DepartureTime = time;
            new_uc_seatmap.TripID = triplist[index].Item5;
            staffmenu.show_btnUCBack();
            staffmenu.loadUC_SeatMap(new_uc_seatmap);
        }
        public void RenderFullTrip()
        {
            int i = 0;
            for (int r = 0; r < 5; r++)
                for (int c = 0; c < 6; c++)
                {
                    RenderATrip(r, c, DepartTimeList[i]);
                    ++i;
                }
            RenderATrip(5, 0, DepartTimeList[30]);
            RenderATrip(5, 1, DepartTimeList[31]);
        }
        List<String> DepartTimeList = new List<String>();
        public void addDepartTimeList()
        {
            int i;
            for (i = 4; i <= 19; i++)
            {
                DepartTimeList.Add(String.Concat(i.ToString(), ":00"));
                DepartTimeList.Add(String.Concat(i.ToString(), ":30"));
            }
        }
        public void RenderATrip(int row, int col, String DepartTime)
        {
            Grid TripGrid = new Grid();
            String cellname = "cell" + row.ToString() + col.ToString();
            TripGrid.Style = Resources["TripInfo"] as Style;
            TripGrid.Background = Brushes.Transparent;
            TripGrid.Margin = new Thickness(8);
            TripGrid.SetValue(FrameworkElement.NameProperty, cellname);
            TripGrid.RenderTransformOrigin = new Point(0.5, 0.5);
            //tao event cho tung cell
            TripGrid.AddHandler(Grid.MouseEnterEvent, new RoutedEventHandler(mygridMouseEnter));
            TripGrid.AddHandler(Grid.MouseLeaveEvent, new RoutedEventHandler(mygridMouseLeave));
            TripGrid.AddHandler(Grid.PreviewMouseDownEvent, new RoutedEventHandler(tripgridPreviewMouseDown));
            //tao grid.rendertransform cho tung cell
            ScaleTransform st = new ScaleTransform();
            st.SetValue(FrameworkElement.NameProperty, "TripInfoTransform");
            st.ScaleX = 1;
            st.ScaleY = 1;
            TripGrid.RenderTransform = st;
            Grid.SetRow(TripGrid, row);
            Grid.SetColumn(TripGrid, col);
            //define column
            RowDefinition rowdef1 = new RowDefinition();
            RowDefinition rowdef2 = new RowDefinition();
            rowdef1.Height = new GridLength(1, GridUnitType.Star);
            rowdef2.Height = new GridLength(3, GridUnitType.Star);
            TripGrid.RowDefinitions.Add(rowdef1);
            TripGrid.RowDefinitions.Add(rowdef2);
            // child grid to trip grid
            Grid TripChildGrid = new Grid();
            Grid.SetRow(TripChildGrid, 0);
            //ben trong tripchildgrid, dong 0
            Border StartTimeBorder = new Border();
            StartTimeBorder.SetValue(FrameworkElement.NameProperty, cellname);
            StartTimeBorder.CornerRadius = new CornerRadius(10);
            RenderOptions.SetEdgeMode(StartTimeBorder, EdgeMode.Aliased);
            StartTimeBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#c9e20b"));
            StartTimeBorder.Width = 70;
            //ben trong StartTimeBorder
            TextBlock tbStartTime = new TextBlock();
            tbStartTime.SetValue(FrameworkElement.NameProperty, cellname);
            tbStartTime.Text = DepartTime;
            tbStartTime.HorizontalAlignment = HorizontalAlignment.Center;
            tbStartTime.VerticalAlignment = VerticalAlignment.Center;
            //them textblock vao border
            StartTimeBorder.Child = tbStartTime;
            //Them StartTimeBorder vao Grid dong 0
            TripChildGrid.Children.Add(StartTimeBorder);
            //tao border markup dong 1       <Border Grid.Row="1">
            Border TripChildBorder = new Border();
            TripChildBorder.SetValue(FrameworkElement.NameProperty, cellname);
            Grid.SetRow(TripChildBorder, 1);
            //ben trong border dong 1
            Grid InfoGrid = new Grid();
            RowDefinition rowdef11 = new RowDefinition();
            RowDefinition rowdef12 = new RowDefinition();
            ColumnDefinition coldef11 = new ColumnDefinition();
            ColumnDefinition coldef12 = new ColumnDefinition();
            InfoGrid.RowDefinitions.Add(rowdef11);
            InfoGrid.RowDefinitions.Add(rowdef12);
            InfoGrid.ColumnDefinitions.Add(coldef11);
            InfoGrid.ColumnDefinitions.Add(coldef12);
            //Border driver 0 0   <Border   Grid.Row="0" Grid.Column="0"   CornerRadius="30,1,1,1" RenderOptions.EdgeMode="Aliased" Background="#82a8e5">
            Border DriverBorder = new Border();
            DriverBorder.SetValue(FrameworkElement.NameProperty, cellname);
            Grid.SetRow(DriverBorder, 0);
            Grid.SetColumn(DriverBorder, 0);
            DriverBorder.CornerRadius = new CornerRadius(30, 1, 1, 1);
            RenderOptions.SetEdgeMode(DriverBorder, EdgeMode.Aliased);
            DriverBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#82a8e5"));
            //textblock ben trong DriverBorder  <TextBlock  Text="Driver" HorizontalAlignment="Center"  VerticalAlignment="Center"></TextBlock>
            TextBlock tbDriver = new TextBlock();
            tbDriver.SetValue(FrameworkElement.NameProperty, String.Concat(cellname,"n"));
            tbDriver.HorizontalAlignment = HorizontalAlignment.Center;
            tbDriver.VerticalAlignment = VerticalAlignment.Center;
            tbDriver.Text = "Driver";
            tbDriverList.Add(tbDriver);
            //gan tbDriver vao DriverBorder
            DriverBorder.Child = tbDriver;
            //License Plate dong 01
            //tao border license plate  <Border Grid.Row ="0" Grid.Column = "1"  CornerRadius="1,30,1,1"  RenderOptions.EdgeMode="Aliased" Background="#236ce0">
            Border LicensePlateBorder = new Border();
            LicensePlateBorder.SetValue(FrameworkElement.NameProperty, cellname);
            Grid.SetRow(LicensePlateBorder, 0);
            Grid.SetColumn(LicensePlateBorder, 1);
            LicensePlateBorder.CornerRadius = new CornerRadius(1, 30, 1, 1);
            RenderOptions.SetEdgeMode(LicensePlateBorder, EdgeMode.Aliased);
            LicensePlateBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#236ce0"));
            //LIcensePlate textblock        <TextBlock  Text="License Plate" HorizontalAlignment="Center" VerticalAlignment="Center"></TextBlock>
            TextBlock tbLicensePlate = new TextBlock();
            tbLicensePlate.SetValue(FrameworkElement.NameProperty, String.Concat(cellname,"p"));
            tbLicensePlate.Text = "License Plate";
            tbLicensePlate.HorizontalAlignment = HorizontalAlignment.Center;
            tbLicensePlate.VerticalAlignment = VerticalAlignment.Center;
            tbPlateList.Add(tbLicensePlate);
            //Gan license plate textblock vao licenseplate border
            LicensePlateBorder.Child = tbLicensePlate;
            //SeatBorder dong 1 0                         <Border Grid.Row = "1" Grid.Column="0" Grid.ColumnSpan="2"  CornerRadius="0,0,30,30"  RenderOptions.EdgeMode="Aliased" Background="#a7efb0">
            Border SeatBorder = new Border();
            SeatBorder.SetValue(FrameworkElement.NameProperty, cellname);
            Grid.SetRow(SeatBorder, 1);
            Grid.SetColumn(SeatBorder, 0);
            Grid.SetColumnSpan(SeatBorder, 2);
            SeatBorder.CornerRadius = new CornerRadius(1, 1, 30, 30);
            RenderOptions.SetEdgeMode(SeatBorder, EdgeMode.Aliased);
            SeatBorder.Background = (Brush)(new BrushConverter().ConvertFrom("#a7efb0"));
            //textblock ben trong SeatBorder
            TextBlock tbSeat = new TextBlock();
            tbSeat.SetValue(FrameworkElement.NameProperty, String.Concat(cellname,"s"));
            tbSeat.HorizontalAlignment = HorizontalAlignment.Center;
            tbSeat.VerticalAlignment = VerticalAlignment.Center;
            tbSeat.Text = "30";
            tbSeat.FontSize = 25;
            tbSeatList.Add(tbSeat);
            //them vao ben trong SeatBorder
            SeatBorder.Child = tbSeat;
            //gan DriverBorder, LicensePlate, SeatBorder vao InfoGrid
            InfoGrid.Children.Add(DriverBorder);
            InfoGrid.Children.Add(LicensePlateBorder);
            InfoGrid.Children.Add(SeatBorder);
            //Gan InfoGrid vao Border dong 1 ngoai cung
            TripChildBorder.Child = InfoGrid;
            //Tao Grid.RenderTransform
            //Gan cac the con vao TripGrid
            TripGrid.Children.Add(TripChildGrid);
            TripGrid.Children.Add(TripChildBorder);
            //Gan Grid vao window
            mygrid.Children.Add(TripGrid);
            //them event mouse enter cho mygrid

        }
        void mygridMouseEnter(object sender, RoutedEventArgs e)
        {
            var cell = e.Source as FrameworkElement;
            ScaleTransform st = new ScaleTransform();
            st.ScaleX = 1.1;
            st.ScaleY = 1.1;
            cell.RenderTransform = st;
        }
        void mygridMouseLeave(object sender, RoutedEventArgs e)
        {
            var cell = e.Source as FrameworkElement;
            ScaleTransform st = new ScaleTransform();
            st.ScaleX = 1;
            st.ScaleY = 1;
            cell.RenderTransform = st;
        }
        private void refreshData(string place, string date)
        {
            triplist = bus_trip.loadTrip_PlaceDate(place, date);
        }
        public string mapPlace(int i)
        {
            if (i == 0)
                return "HCMC";
            return "Ben Tre";
        }
        private void loadSeatNo()
        {
            int i = 0;
            foreach (TextBlock tb in tbSeatList)
            {
                tb.Text = triplist[i++].Item3.ToString();
            }
        }
        private void loadDriverName()
        {
            int i = 0;
            foreach (TextBlock tb in tbDriverList)
            {
                tb.Text = triplist[i++].Item1.ToString();
            }
        }
        private void loadPlate()
        {
            int i = 0;
            foreach (TextBlock tb in tbPlateList)
            {
                tb.Text = triplist[i++].Item2.ToString();
            }
        }
        private void loadDBTrip(object sender, RoutedEventArgs e)
        {
            if (cbbDepartPlace.Text == "" || dpDate.Text == "")
            {
                MessageBox.Show("Please choose input information!!!");
            }
            else
            {
                triplist = new List<Tuple<string,string,int, DTO_Trip, int>>( bus_trip.loadTrip_PlaceDate(mapPlace(cbbDepartPlace.SelectedIndex), dpDate.Text));
                loadDriverName();
                loadPlate();
                loadSeatNo();
            }
        }

        private void UC_TripList_Load(object sender, RoutedEventArgs e)
        {
            cbbDepartPlace.SelectedIndex = 0;
            DateTime dt = DateTime.Now;
            dpDate.Text = dt.ToString();
            triplist = new List<Tuple<string, string, int, DTO_Trip, int>>(bus_trip.loadTrip_PlaceDate(mapPlace(cbbDepartPlace.SelectedIndex), dpDate.Text));
            loadDriverName();
            loadPlate();
            loadSeatNo();
        }
    }
}
