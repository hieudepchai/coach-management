﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DTO;
namespace GUI
{
    /// <summary>
    /// Interaction logic for GUI_popup_psginfo.xaml
    /// </summary>
    public partial class GUI_popup_psginfo : Window
    {
        public DTO_Passenger psg;
        public string str_seatno;
        public GUI_popup_psginfo()
        {
            InitializeComponent();
        }
        private void btnClose_click(object sender, RoutedEventArgs e)
        {
            Close();

        }

        private void btnClose_mouseenter(object sender, MouseEventArgs e)
        {
            btnClose.Background = (Brush)new BrushConverter().ConvertFrom("#96f74c7f");
        }

        private void btnClose_MouseLeave(object sender, MouseEventArgs e)
        {
            btnClose.Background = (Brush)new BrushConverter().ConvertFrom("#FFDCC0C0");
        }

        private void loadSeatNo(object sender, RoutedEventArgs e)
        {
            var thisTextBlock = e.Source as TextBlock;
            thisTextBlock.Text = str_seatno;
            if (psg != null)
            {
                tbFullName.Text = psg.getFullName();
                tbBirthYear.Text = psg.getBirthYear().ToString();
                tbPhoneNumber.Text = psg.getPhonenumber();
                tbBookingDate.Text = psg.getCreatedDate();
            }

        }
    }
}
