﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;
using System.Collections;
using System.Data.Linq;
using System.Windows;
using System.Windows.Forms;

namespace DAL
{
    public class DAL_Bonus:DAL_ConnectDB
    {
        public bool addBonus(DTO_Bonus bns)
        {
            try
            {
                conn.Open();
                string SQL = string.Format("insert into Bonus (StaffID, BonusAmount, ValidDate, Description) values ('{0}', '{1}', '{2}', '{3}')"
                , bns.getStaffID(), bns.getBonusAmount(), bns.getValidDate(), bns.getDescription());
                SqlCommand cmd = new SqlCommand(SQL, conn);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }
            return false;
        }
        public int editBus(DTO_Bonus bns)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("update Bonus set BonusAmount = '{0}',ValidDate = '{1}',Description='{2}' where BonusID ='{3}'"
                , bns.getBonusAmount(), bns.getValidDate(), bns.getDescription(),bns.getBonusID());
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public int deleteBus(int id)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("delete from Bonus where BonusID = '{0}'", id);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public DataTable getBonus()
        {
            DataTable dtbonus = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("select * from bonus", conn);
            DA.Fill(dtbonus);
            return dtbonus;
        }

    }
}

