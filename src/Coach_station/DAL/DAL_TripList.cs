﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;
using System.Collections;
using System.Data.Linq;
using System.Windows;
using System.Windows.Forms;

namespace DAL
{
    public class DAL_TripList : DAL_ConnectDB
    {
        public List<DTO_TripTable> CreateSet()
        {
            List<DTO_TripTable> TripList = new List<DTO_TripTable>();
            try
            {
                conn.Open();
                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Staff,(select * from Bus where Staff.PositionID=2)", conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    DTO_Bus bus = new DTO_Bus((int)reader["Bus.BusID"], (string)reader["Bus.LiscencePlate"], (string)reader["Bus.BrandName"], (string)reader["Bus.BoughtDate"]);
                    DTO_Staff staff = new DTO_Staff((int)reader["Staff.StaffID"], (string)reader["Staff.FullName"], (string)reader["Staff.DOB"], (string)reader["Staff.PhoneNumber"],
                        (string)reader["Staff.Address"], (int)reader["Staff.PositionID"], (string)reader["Staff.StartDate"], (float)reader["Staff.Salary"]);
                    DTO_TripTable trip = new DTO_TripTable(bus, staff);
                    TripList.Add(trip);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Cannot create!");
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }
            return TripList;
        }
    }
}
