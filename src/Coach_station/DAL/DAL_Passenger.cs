﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;
using System.Collections;
using System.Data.Linq;
using System.Windows;
using System.Windows.Forms;


namespace DAL
{
    public partial class DAL_Passenger : DAL_ConnectDB
    {
        public bool addPassenger(DTO_Passenger passenger)
        {
            try
            {
                conn.Open();
               string SQL = string.Format("insert into Passenger (Fullname, BirthYear, PhoneNumber, CreatedDate) values ('{0}', '{1}', '{2}', '{3}')"
               ,passenger.getFullName(), passenger.getBirthYear(), passenger.getPhonenumber(), passenger.getCreatedDate());
                SqlCommand cmd = new SqlCommand(SQL, conn);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Can't add new passenger");
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }
            return false;
        }
        public int editPassenger(DTO_Passenger passenger)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("update Passenger set Fullname = '{0}', BirthYear = '{1}',PhoneNumber = '{2}',CreatedDate ='{3}' where PassengerID ='{4}'",
                     passenger.getFullName(), passenger.getBirthYear(), passenger.getPhonenumber(), passenger.getCreatedDate(),passenger.getPassengerID());
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public int deletePassenger(int id)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("delete from Passenger where PassengerID = '{0}'", id);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public DataTable getPassenger()
        {
            DataTable dt = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("select * from Passenger", conn);
            DA.Fill(dt);
            return dt;
        }


        public int getExistPassengerID(string PhoneN)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("select * from passenger where PhoneNumber = '{0}'", PhoneN);
                SqlCommand sqlCommand = new SqlCommand(strCmd, conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.Read())
                    return (int)reader["PassengerID"];

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
    }
}





