﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;
using System.Windows.Forms;

namespace DAL
{
    public class DAL_Trips : DAL_ConnectDB
    {
        public List<DTO.TripsInfor> getListTripsInfor(string date, string DeptPlace)
        {
            List<DTO.TripsInfor> listTrips = new List<DTO.TripsInfor>();
            try
            {
                conn.Open();
                string sql = string.Format("select TripID, DepartPlace, DepartureTime, Date, Type, Price from trip, TicketPrice where trip.PriceID = TicketPrice.PriceID and trip.Date = '{0}' and DepartPlace = '{1}' ", date, DeptPlace);
                SqlCommand sqlCommand = new SqlCommand(sql, conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    DTO.TripsInfor OneTrip = new TripsInfor((int)reader["TripID"], (string)reader["DepartPlace"], (string)reader["DepartureTime"], reader["Date"].ToString(),
                        (int)reader["Price"], (string)reader["Type"]);
                    listTrips.Add(OneTrip);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }

            return listTrips;
        }

        public int getTripID(string DepTime, string DepPlace, string date)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("select * from trip where DepartPlace = '{0}' and DepartureTime = '{1}' and Date = '{2}'", DepPlace, DepTime, date);
                SqlCommand sqlCommand = new SqlCommand(strCmd, conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                if (reader.Read())
                    return (int)reader["TripID"];

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
    }
}
