﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DTO;

namespace DAL
{
    public class Tickets_analysis :DAL_ConnectDB
    {
        public List<DTO.Tickets_analysis> getnTicket(string Startdate, string EndDate)
        {
            List<DTO.Tickets_analysis> listnTicketsPerTime = new List<DTO.Tickets_analysis>();
            try
            {
                conn.Open();
                string sql = string.Format("select Trip.DepartureTime, count(booking.SeatNo) as nTicket from booking, Trip where Trip.TripID = booking.TripID and Trip.Date between '{0}' and '{1}' group by Trip.DepartureTime ", Startdate, EndDate);
                SqlCommand sqlCommand = new SqlCommand(sql, conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    DTO.Tickets_analysis nTicketPerTime = new DTO.Tickets_analysis((int)reader["nTicket"], (string) reader["DepartureTime"]);
                    listnTicketsPerTime.Add(nTicketPerTime);
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }

            return listnTicketsPerTime;
        }
    }
}
