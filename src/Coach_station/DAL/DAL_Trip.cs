﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DTO;


namespace DAL
{
    public class DAL_Trip:DAL_ConnectDB
    {
        public bool addTrip(DTO_Trip trip)
        {
            try
            {
                conn.Open();
                string SQL = string.Format("insert into Trip(DepartPlace,DepartureTime,Date,PriceID,Driver,BusID,Type) values ('{0}', '{1}', '{2}','{3}','{4}','{5}','{6}')"
                , trip.getDepPlace(), trip.getDepTime(), trip.getDate(), trip.getPriceID(), trip.getDriverID(), trip.getBusID(), trip.getType());
                SqlCommand cmd = new SqlCommand(SQL, conn);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }
            return false;
        }


        public void generateDailyTrips(string date)
        {           
            List<Tuple<string, string>> PairList = new List<Tuple<string, string>>();
            List<Tuple<string, string, string,string,string>> scheduleList = new List<Tuple<string, string, string,string,string>>();
            DAL_Pair dal_pair = new DAL_Pair();
            PairList = dal_pair.getTupleList();
            int j = 0;
            int z = 8;
            for (double i = 4; i <= 19.5; i += 0.5)
            {
                Tuple<string, string, string, string, string> tempTuple = new Tuple<string, string, string, string, string>(String.Concat(((int)i).ToString(), mymap(i - (int)i)), PairList[j].Item1, PairList[j].Item2, PairList[z].Item1, PairList[z].Item2);
                scheduleList.Add(tempTuple);
                if (j == 15) j = 0;
                else ++j;
                if (z == 15) z = 0;
                else ++z;
            }
            foreach (Tuple<string, string, string, string, string> item in scheduleList)
            {
                //DAL_Trip dal_trip = new DAL_Trip();
                DTO_Trip trip1 = new DTO_Trip("HCMC",item.Item1,date,1,Int32.Parse(item.Item2),Int32.Parse(item.Item3),"Normal");
                addTrip(trip1);
                DTO_Trip trip2 = new DTO_Trip("Ben Tre", item.Item1, date, 1, Int32.Parse(item.Item4), Int32.Parse(item.Item5), "Normal");
                addTrip(trip2);
            }
        }
        public List<Tuple<string,string,int,DTO_Trip, int>> loadTrip_PlaceDate(string place, string date)
        {
           List<Tuple<string,string,int, DTO_Trip, int>> ListOfTrips = new List<Tuple<string,string, int, DTO_Trip, int>>();
            DAL_Bus dal_bus = new DAL_Bus();
            DAL_Staff dal_staff = new DAL_Staff();
            try
            {
                conn.Open();
                string stm = string.Format("select * from Trip  where DepartPlace = '{0}' and Date = '{1}'", place, date);
                SqlCommand sqlcommand = new SqlCommand(stm, conn);
                SqlDataReader reader = sqlcommand.ExecuteReader();
                while (reader.Read())
                {
                    string bus_plate ="";
                    string driver_name="";
                    int count = 0;
                    Tuple<string,string, int, DTO_Trip, int> tempTuple;
                    DTO_Trip trip = new DTO_Trip((int)reader["TripID"], reader["DepartPlace"].ToString(), reader["DepartureTime"].ToString(), reader["Date"].ToString(), (int)reader["PriceID"], (int)reader["Driver"], (int)reader["BusID"], reader["Type"].ToString());
                    bus_plate = dal_bus.getPlate(trip.getBusID());
                    driver_name = dal_staff.getDriverName(trip.getDriverID());
                    string stm2 = string.Format("select count(*) as Num from Booking where TripID = '{0}'", trip.getTripID());
                    SqlCommand sqlcommand2 = new SqlCommand(stm2, conn);
                    SqlDataReader reader2 = sqlcommand2.ExecuteReader();
                    if (reader2.Read())
                    {
                        count = (int)reader2["Num"];
                    }
                    else
                        return null;
                    tempTuple = new Tuple<string,string, int, DTO_Trip, int>(driver_name,bus_plate, count,trip, trip.getTripID());
                    ListOfTrips.Add(tempTuple);
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
            return ListOfTrips;
        }
        private string mymap(double d)
        {
            if (d == 0)
                return ":00";
            return ":30";
        }


    }
}

