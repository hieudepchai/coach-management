﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;
using System.Collections;
using System.Data.Linq;
using System.Windows;
using System.Windows.Forms;
namespace DAL
{
    public class DAL_Booking : DAL_ConnectDB
    {

        public bool addBooking(DTO.DTO_Booking book)
        {
            try
            {
                conn.Open();
                string SQL = string.Format("insert into Booking (TripID, SeatNo, PassengerID, CreatedDate) values ('{0}', '{1}', '{2}', '{3}')"
                ,book.getTripID(), book.getSeatNo(), book.getPassengerID(), book.getCreatedDate());
                SqlCommand cmd = new SqlCommand(SQL, conn);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }
            return false;
        }
        public int editBooking(DTO_Booking booking)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("update Booking set SeatNo = '{0}',PassengerID = '{1}',CreatedDate='{2}' where TripID ='{3}'",
                      booking.getSeatNo(), booking.getPassengerID(), booking.getCreatedDate(), booking.getTripID());
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public int deleteBus(int id)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("delete from Booking where TripID = '{0}'", id);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public DataTable getBooking()
        {
            DataTable dtbooking = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("select * from Booking", conn);
            DA.Fill(dtbooking);
            return dtbooking;
        }

        public List<int> isSelectedSeat (int TripID)
        {
            List<int> ListSeat = new List<int>();
            try
            {
                
                conn.Open();
                string strCmd = string.Format("select SeatNo from Booking where TripID = '{0}'", TripID);
                SqlCommand sqlCommand = new SqlCommand(strCmd, conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                    ListSeat.Add((int)reader["SeatNo"]);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return ListSeat;
        }
        public List< Tuple<DTO_Passenger, int>> getPsgandSeatList(int TripID)
        {
            List<Tuple<DTO_Passenger, int>> resList = new List<Tuple<DTO_Passenger, int>>();
            try
            {

                conn.Open();
                string strCmd = string.Format("select p.PassengerID, b.SeatNo, p.FullName,p.BirthYear, p.PhoneNumber, p.CreatedDate from Booking b, Passenger p where TripID = '{0}' and p.PassengerID = b.PassengerID", TripID);
                SqlCommand sqlCommand = new SqlCommand(strCmd, conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    DTO_Passenger psg = new DTO_Passenger((int)reader["PassengerID"], reader["FullName"].ToString(), (int)reader["BirthYear"], reader["PhoneNumber"].ToString(), reader["CreatedDate"].ToString());
                    Tuple<DTO_Passenger, int> tempTuple = new Tuple<DTO_Passenger, int>(psg,(int)reader["SeatNo"]);
                    resList.Add(tempTuple);
                }
            }

            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return resList;
        }
    }
}
