﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DTO;
namespace DAL
{
    public class DAL_Pair:DAL_ConnectDB
    {
        public int editPair(DTO_Pair pair)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("update Pair set StaffID = '{0}', BusID = '{1}' where PairID = '{2}'",pair.DriverID,pair.BusID,pair.PairID);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public List<Tuple<string,string>> getTupleList()
        {
            List<Tuple<string, string>> TupleList = new List<Tuple<string, string>>();
            try
            {
                conn.Open();
                SqlCommand sqlcommand = new SqlCommand("SELECT StaffID, BusID from Pair ", conn);
                SqlDataReader reader = sqlcommand.ExecuteReader();
                int i = 0;
                while (reader.Read())
                {
                    TupleList.Add(new Tuple<string, string>(reader["StaffID"].ToString(), reader["BusID"].ToString()));
                    ++i;
                }
            }
                catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                conn.Close();
            }
       
            return TupleList;
        }
        public DataTable getPair()
        {
            DataTable dt = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("select * from Pair", conn);
            DA.Fill(dt);
            return dt;
        }
    }

}
