﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;
using System.Collections;
using System.Data.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace DAL
{
    public class DAL_Account: DAL_ConnectDB
    {
        public bool addAccount(DTO_Account account)
        {
            try
            {
                string password = account.getPassword();
                using (MD5 md5Hash = MD5.Create())
                {
                    string hash = GetMd5Hash(md5Hash, password);
                    conn.Open();
                    string SQL = string.Format("insert into Account (Username, Password, StaffID) values ('{0}', '{1}', '{2}')"
                    , account.getUsername(), hash, account.getStaffID());
                    SqlCommand cmd = new SqlCommand(SQL, conn);
                    if (cmd.ExecuteNonQuery() > 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Can't add new Account");
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }
            return false;
        }
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        // Verify a hash against a string.
        static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GetMd5Hash(md5Hash, input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int editAccount(DTO_Account account)
        {
            try
            {
                    conn.Open();
                    string strCmd = string.Format("update Account set Username = '{0}', Password = '{1}' where StaffID ='{2}'",
                         account.getUsername(), account.getPassword(), account.getStaffID());
                    SqlCommand cmd = new SqlCommand(strCmd, conn);
                    if (cmd.ExecuteNonQuery() > 0) return 1;
                
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public int deleteAccount(int id)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("delete from Account where StaffID = '{0}'", id);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }

        public DTO_Account checkAccount(string userName, string password)
        {
            DTO_Account account;
            try
            {
                conn.Open();
                string SQL = string.Format("select * from Account where Username = '{0}'", userName);
                SqlCommand cmd2 = new SqlCommand(SQL, conn);
                SqlDataReader reader = cmd2.ExecuteReader();
                if (reader.Read())
                {
                    using (MD5 md5Hash = MD5.Create())
                    {
                        if (VerifyMd5Hash(md5Hash, password, (string)reader["Password"]))
                        {
                            account = new DTO_Account((string)reader["Username"], (string)reader["Password"]);
                            return account;
                        }
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Cannot connect to the database!!!");
            }
            finally
            {
                conn.Close();
            }
            return null;
        }

    }
}
