﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DTO;
using System.Windows.Forms;

namespace DAL
{
    public class Finance_analysis : DAL_ConnectDB
    {
        public List<DTO.Finance_analysis> getnFinance(string Startdate, string EndDate)
        {
            List<DTO.Finance_analysis> listnFinancePerTime = new List<DTO.Finance_analysis>();
            try
            {
                conn.Open();
                string sql = string.Format("select Trip.DepartureTime, sum(TicketPrice.Price) as TotalPrice from booking, TicketPrice, Trip where Trip.TripID = booking.TripID and Trip.PriceID = TicketPrice.PriceID and trip.Date between '{0}' and '{1}' group by Trip.DepartureTime ", Startdate, EndDate);
                SqlCommand sqlCommand = new SqlCommand(sql, conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    DTO.Finance_analysis FinancePerTime = new DTO.Finance_analysis((int)reader["TotalPrice"], (string)reader["DepartureTime"]);
                    listnFinancePerTime.Add(FinancePerTime);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }

            return listnFinancePerTime;
        }

        public List<string> getTime()
        {
            List<string> times = new List<string>();
            try
            {
                conn.Open();
                string sql = string.Format("select distinct DepartureTime from trip");
                SqlCommand sqlCommand = new SqlCommand(sql, conn);
                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader.Read())
                {
                    times.Add((string)reader["DepartureTime"]);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }

            return times;
        }
    }
}
