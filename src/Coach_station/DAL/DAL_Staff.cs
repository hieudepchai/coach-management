﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;
using System.Collections;
using System.Data.Linq;
using System.Windows.Forms;

namespace DAL
{
    public class DAL_Staff : DAL_ConnectDB
    {
        private DAL_Account account = new DAL_Account();
        public bool addStaff(DTO_Staff staff)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("insert into Staff ( FullName, DOB, PhoneNumber, Address, PositionID, StartDate,Salary) values ('{0}','{1}',N'{2}','{3}','{4}','{5}','{6}')"
                    , staff.getFullName(), staff.getDOB(), staff.getPhonenumber(), staff.getAddress(), staff.getPositionID(), staff.getStartDate(), staff.getSalary());
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;

            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot connect to the database!!!");
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public int deleteStaff(int ID)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("delete from Staff where StaffID = {0}", ID);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                Console.WriteLine("Connection Failed!!!");

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public int editStaff(DTO_Staff staff)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("update Staff set FullName = '{0}', DOB = N'{1}', PhoneNumber = '{2}', Address = '{3}', PositionID = '{4}', StartDate = '{5}',Salary ='{6}' where StaffID ='{7}'"
                    , staff.getFullName(), staff.getDOB(), staff.getPhonenumber(), staff.getAddress(), staff.getPositionID(), staff.getStartDate(), staff.getSalary(), staff.getStaffID());
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                Console.WriteLine("Connection Failed!!!");
            }
            finally
            {
                conn.Close();
            }
            return 0;
        }

        public DataTable getStaff()
        {
            DataTable dtStaff = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("select * from Account ,Staff where Account.StaffID = Staff.StaffID and Staff.PositionID=1", conn);
            DA.Fill(dtStaff);
            return dtStaff;
        }
        public DataTable getDriver()
        {
            DataTable dtDriver = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("select* from Staff where PositionID = 2", conn);
            DA.Fill(dtDriver);
            return dtDriver;
        }
        public string getDriverName(int driverid)
        {
            string res = "";
            try
            {
                conn.Open();
                string strCmd = string.Format("select FullName from Staff where StaffID = {0}", driverid);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    res = reader["FullName"].ToString();

            }
            catch (Exception e)
            {
                Console.WriteLine("Connection Failed!!!");

            }
            finally
            {
                conn.Close();
            }
            return res;
        }
        public int getStaffID(string staffname)
        {
            int res=0;
            try
            {
                conn.Open();
                string strCmd = string.Format("select StaffID from Staff where FullName = '{0}'", staffname);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    res = (int)reader["StaffID"];
            }

            catch (Exception e)
            {
                Console.WriteLine("Connection Failed!!!");
            }
            finally
            {
                conn.Close();
            }
            return res;
        }
    }
}
