﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;
using System.Collections;
using System.Data.Linq;
using System.Windows;
using System.Windows.Forms;

namespace DAL
{
    public class DAL_Bus : DAL_ConnectDB
    {
        public bool addBus(DTO_Bus bus)
        {
            try
            {
                conn.Open();
                string SQL = string.Format("insert into Bus (LiscencePlate, BrandName, BoughtDate) values ('{0}', '{1}', '{2}')"
                , bus.getLiscensePlate(), bus.getBrandName(), bus.getBoughtDate());
                SqlCommand cmd = new SqlCommand(SQL, conn);
                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                // Dong ket noi
                conn.Close();
            }
            return false;
        }
        public int editBus(DTO_Bus bus)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("update Bus set LiscencePlate = '{0}', BrandName = '{1}',BoughtDate = '{2}' where BusID ='{3}'",
                     bus.getLiscensePlate(), bus.getBrandName(), bus.getBoughtDate(), bus.getBusID());
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public int deleteBus(int id)
        {
            try
            {
                conn.Open();
                string strCmd = string.Format("delete from Bus where BusID = '{0}'", id);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                if (cmd.ExecuteNonQuery() > 0) return 1;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return 0;
        }
        public DataTable getBus()
        {
            DataTable dtbus = new DataTable();
            SqlDataAdapter DA = new SqlDataAdapter("select * from Bus", conn);
            DA.Fill(dtbus);
            return dtbus;
        }
        public string getPlate(int busid)
        {
            string res = "";
            try
            {
                conn.Open();
                string strCmd = string.Format("select LiscencePlate from Bus where BusID = '{0}'", busid);
                SqlCommand cmd = new SqlCommand(strCmd, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                    res = reader["LiscencePlate"].ToString();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);

            }
            finally
            {
                conn.Close();
            }
            return res;
        }
    }
}

