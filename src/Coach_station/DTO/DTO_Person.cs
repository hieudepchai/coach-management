﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Person
    {
        //private string ID;
        private string FullName;
        private string PhoneNumber;

        public DTO_Person()
        {

        }

        public DTO_Person(string fn, string pn )
        {
            FullName = fn;
            PhoneNumber = pn;
        }
        //get set full name
        public string getFullName() { return FullName; }
        public void setFullName(string fn) { FullName = fn; }
        //get set phonenumber
        public string getPhonenumber() { return PhoneNumber; }
        public void setPhoneNumber(string pn) { PhoneNumber = pn; }

    }
}
