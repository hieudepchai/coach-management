﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Account
    {
        private int AccountID;
        private string UserName;
        private string Password;
        private int StaffID;
        public DTO_Account(string un,string pw,int staffid) 
        {
            this.UserName = un;
            this.Password = pw;
            this.StaffID = staffid;
        }
        public DTO_Account(string un, string pw )
        {
            this.UserName = un;
            this.Password = pw;
        }

        public DTO_Account(int id,string un, string pw, int staffid)
        {
            this.AccountID = id;
            this.UserName = un;
            this.Password = pw;
            this.StaffID = staffid;
        }

        //get set account id
        public void setAccountID(int id) { this.AccountID = id; }
        public int getAccountID() { return this.AccountID; }

        //get set username
        public string getUsername() { return this.UserName; }
        public void setUserName(string un) { this.UserName = un; }

        //get set password
        public string getPassword() { return this.Password; }
        public void setPassword(string pw) { this.Password = pw; }

        //get set staff id
        public int getStaffID() { return this.StaffID; }
        public void setStaffID(int staffid) { this.StaffID = staffid; }

    }
}
