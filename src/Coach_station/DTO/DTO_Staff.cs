﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Staff : DTO_Person
    {
        private int StaffID;
        private string DOB;
        private String Address;
        private int PositionID;
        private string StartDate;
        private float Salary;
        public DTO_Staff() { }
        public DTO_Staff(string fn, string dob, string phone, string address, int posID, string startdate, float salary) : base(fn, phone)
        {
            DOB = dob;
            Address = address;
            PositionID = posID;
            StartDate = startdate;
            Salary = salary;
        }
        public DTO_Staff(int id,string fn, string dob, string phone, string address, int posID, string startdate, float salary) : base(fn, phone)
        {
            StaffID = id;
            DOB = dob;
            Address = address;
            PositionID = posID;
            StartDate = startdate;
            Salary = salary;
        }
        //get set staff id
        public int getStaffID() { return this.StaffID; }
        public void setStaffID(int id) { this.StaffID = id; }

        //get set date of birth
        public string getDOB() { return this.DOB; }
        public void setDOB(string dob) { DOB = dob; }

        //get set address
        public String getAddress() { return this.Address; }
        public void setAddress(string address) { Address = address; }

        //get set position id
        public int getPositionID() { return this.PositionID; }
        public void setPositionID(int posID) { PositionID = posID; }

        //get set start date
        public string getStartDate() { return this.StartDate; }
        public void setStartDate(string startdate) { StartDate = startdate; }

        //get set salary
        public float getSalary() { return this.Salary; }
        public void setSalary(float sal) { Salary = sal; }
    }
}
