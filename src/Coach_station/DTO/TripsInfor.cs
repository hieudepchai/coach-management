﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class TripsInfor
    {
        private int TripID;
        private string DepPlace;
        private string DepTime;
        private string Date;
        private int Price;
        private string Type;

        public TripsInfor(int tripID, string DepPlace, string Deptime, string date, int price, string type)
        {
            this.TripID = tripID;
            this.DepPlace = DepPlace;
            this.DepTime = Deptime;
            this.Date = date;
            this.Price = price;
            this.Type = type;
        }

        //get set trip id
        public int getTripID() { return this.TripID; }

        public string getDepPlace() { return this.DepPlace; }
        public void setDepPlace(string DepPlace) { this.DepPlace = DepPlace; }

        public string getDepTime() { return this.DepTime; }
        public void setDepTime(string DepTime) { this.DepTime = DepTime; }

        //get set date
        public string getDate() { return this.Date; }
        public void setDate(string d) { this.Date = d; }

        //get set price
        public int getPrice() { return this.Price; }
        public void setPriceID(int price) { this.Price = price; }

        public string getType() { return this.Type; }
        public void setType(string type) { this.Type = type; }
    }
}
