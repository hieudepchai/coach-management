﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Bonus
    {
        private int BonusID;
        private float BonusAmount;
        private string ValidDate;
        private string Description;
        private int StaffID;
        public DTO_Bonus(int stid,float amount, string date, string description)
        {
            this.StaffID = stid;
            this.BonusAmount = amount;
            this.ValidDate = date;
            this.Description = description;
        }

        public DTO_Bonus(int bid,int staffid, float amount,string date,string description) 
        {
            this.BonusID = bid;
            this.BonusAmount = amount;
            this.ValidDate = date;
            this.Description = description;
            this.StaffID = staffid;
        }
        //get set bonus id
        public void setBonusID(int id) { this.BonusID = id; }
        public int getBonusID() { return this.BonusID; }

        //get set amount
        public float getBonusAmount() { return this.BonusAmount; }
        public void setBonusAmount(float bonus) { this.BonusAmount = bonus; }

        //get set valid date
        public string getValidDate() { return this.ValidDate; }
        public void setValidDate(string date) { this.ValidDate = date; }

        //get set description
        public string getDescription() { return this.Description; }
        public void setDescription(string des) { this.Description = des; }

        //get set Staff ID 
        public int getStaffID() { return this.StaffID; }
        public void setStaffID(int id) { this.StaffID = id; }
    }
}
