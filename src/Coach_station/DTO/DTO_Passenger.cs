﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{

    public class DTO_Passenger:DTO_Person
    {
        private int PassengerID;
        private string CreatedDate; //Kieu dinh dang(YYYY/MM/DD) 
        private int BirthYear;
        public DTO_Passenger() { }
        public DTO_Passenger(string fullN, int birthyear, string nPhone, string date) : base(fullN,nPhone)
        {
            this.BirthYear = birthyear;
            this.CreatedDate = date;
        }
        public DTO_Passenger(int id,string fullN, int birthyear, string nPhone, string date) : base(fullN, nPhone)
        {
            this.PassengerID = id;
            this.BirthYear = birthyear;
            this.CreatedDate = date;
        }
        //get set passenger id
        public void setPassengerID(int id) { this.PassengerID = id; }
        public int getPassengerID() { return this.PassengerID; }
        //get set created date
        public void setCreatedDate(string date)
        {
            this.CreatedDate = date;
        }
        public string getCreatedDate()
        {
            return this.CreatedDate;
        }
        //get set birthyear
        public void setBirthYear(int birthyear)
        {
            this.BirthYear = birthyear;
        }
       public int getBirthYear()
        {
            return this.BirthYear;
        }
    }
}
