﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_TripTable
    {
        private int DriverID;
        private int BusID;
        public DTO_TripTable(DTO_Bus bus1, DTO_Staff staff)
        {
            this.BusID = bus1.getBusID();
            this.DriverID = staff.getStaffID();
        }
        public int getDriverID() { return DriverID; }
        public void setDriverID(int id) { this.DriverID = id; }

        public int getBusID() { return BusID; }
        public void setBusID(int id) { this.BusID = id; }

    }
}
