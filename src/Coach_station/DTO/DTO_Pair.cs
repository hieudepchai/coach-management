﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
     public class DTO_Pair
    {
        public int PairID;
        public int DriverID;
        public int BusID;
          public DTO_Pair(int pairid, int driverid, int busid)
        {
            PairID = pairid;
            DriverID = driverid;
            BusID = busid;
        }
    }
}
