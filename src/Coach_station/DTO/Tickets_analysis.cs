﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Tickets_analysis
    {
        private int nTickets;
        private string DepTime;


        public void SetnTickets(int nTickets) { this.nTickets = nTickets; }
        public int GetnTickets() { return this.nTickets; }
        public void SetDepTime(string DepTime) { this.DepTime = DepTime; }
        public string GetDepTime() { return this.DepTime; }


        public Tickets_analysis() { }
        public Tickets_analysis(int nTickets, string DepTime)
        {
            this.nTickets = nTickets;
            this.DepTime = DepTime;

        }
    }
}
