﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Position
    {
        private int PositionID;
        private string PositionName;
        private float BasicSalary;
        public DTO_Position( string posname,float sal)
        {
            this.PositionName = posname;
            this.BasicSalary = sal;
        }
        //get set position id
        public int getPositionID() { return this.PositionID; }
        public void setPositionID(int id) { this.PositionID = id; }
        //get set position name
        public string getPositionName() { return this.PositionName; }
        public void setPositionName(string posname) { this.PositionName = posname; }

        //get set basic salary
        public float getBasicSalary() { return this.BasicSalary; }
        public void setBasicSalary(float sal) { this.BasicSalary = sal; }
    }
}
