﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_TicketPrice
    {
        private int PriceID;
        private string StartDate;
        private int price;

        public DTO_TicketPrice(string StartDate, int price)
        {
            this.StartDate = StartDate;
            this.price = price;
        }

        public int getPriceID() { return this.PriceID; }
        public string getStartDate() { return this.StartDate; }
        public void setStartDate(string StartDate) { this.StartDate = StartDate; }

        public int getPrice() { return this.price; }
        public void setPrice(int price) { this.price = price; }
    }
}
