﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Bus
    {
        private int BusID;
        private string LiscensePlate;
        private string BrandName;
        private string BoughtDate;

        public DTO_Bus(string liscense, string brand, string boughtdate)
        {
            this.LiscensePlate = liscense;
            this.BrandName = brand;
            this.BoughtDate = boughtdate;
        }
        public DTO_Bus(int id,string liscense, string brand, string boughtdate)
        {
            this.BusID = id;
            this.LiscensePlate = liscense;
            this.BrandName = brand;
            this.BoughtDate = boughtdate;
        }

        //get set bus id
        public int getBusID() { return this.BusID; }
        public void setBusID(int id) { this.BusID = id; }
        //get set liscense plate
        public string getLiscensePlate() { return this.LiscensePlate; }
        public void setLiscensePlate(string lis) { this.LiscensePlate = lis; }

        //get set brand name
        public string getBrandName() { return this.BrandName; }
        public void setBrandName(string brand) { this.BrandName = brand; }

        //get set bought date
        public string getBoughtDate() { return this.BoughtDate; }
        public void setBoughtDate(string date) { this.BoughtDate = date; }

    }
}
