﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Booking
    {
        private int TripID;
        private int SeatNo;
        private int PassengerID;
        private string CreatedDate;
        public DTO_Booking(int seatid,int passid,string date)
        {
            this.SeatNo = seatid;
            this.PassengerID = passid;
            this.CreatedDate = date;

        }

        //get set Trip ID
        public void setTripID(int id) { this.TripID = id; }
        public int getTripID() { return this.TripID; }

        //get set seat id
        public void setSeatNo(int id) { this.SeatNo = id; }
        public int getSeatNo() { return this.SeatNo; }

        //get set created date
        public void setCreatedDate(string date)
        {
            this.CreatedDate = date;
        }
        public string getCreatedDate()
        {
            return this.CreatedDate;
        }

        //get set passenger id
        public void setPassengerID(int passengerid)
        {
            this.PassengerID = passengerid;
        }
        public int getPassengerID()
        {
            return this.PassengerID;
        }

        public DTO_Booking(int TripId, int seatid, int passid, string date)
        {
            this.TripID = TripId;
            this.SeatNo = seatid;
            this.PassengerID = passid;
            this.CreatedDate = date;

        }
    }
}
