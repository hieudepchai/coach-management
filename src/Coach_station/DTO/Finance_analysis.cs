﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Finance_analysis
    {
        private double money;
        private string DepTime;

        public void SetMoney(double money) { this.money = money; }
        public double getMoney() { return this.money; }
        public void SetDepTime(string DepTime) { this.DepTime = DepTime; }
        public string GetDepTime() { return this.DepTime; }

        public Finance_analysis() { }
        public Finance_analysis(double money, string DepTime)
        {
            this.money = money;
            this.DepTime = DepTime;
        }
    }
}
