﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Trip
    {
        private int TripID;
        private string DepPlace;
        private string DepTime;
        private string Date;
        private int DriverID;
        private int PriceID;
        private int BusID;
        private string Type;
        public DTO_Trip(string DepPlace, string Deptime, string date, int priceID, int DriverID, int BusID, string type)
        {
            this.DepPlace = DepPlace;
            this.DepTime = Deptime;
            this.Date = date;
            this.DriverID = DriverID;
            this.PriceID = priceID;
            this.BusID = BusID;
            this.Type = type;
        }
        public DTO_Trip(int id,string DepPlace, string Deptime, string date, int priceID, int DriverID, int BusID, string type)
        {
            this.TripID = id;
            this.DepPlace = DepPlace;
            this.DepTime = Deptime;
            this.Date = date;
            this.DriverID = DriverID;
            this.PriceID = priceID;
            this.BusID = BusID;
            this.Type = type;
        }

        //get set trip id
        public int getTripID() { return this.TripID; }

        public string getDepPlace() { return this.DepPlace; }
        public void setDepPlace(string DepPlace) { this.DepPlace = DepPlace; }

        public string getDepTime() { return this.DepTime; }
        public void setDepTime(string DepTime) { this.DepTime = DepTime; }

        //get set date
        public string getDate() { return this.Date;}
        public void setDate(string d) { this.Date = d; }

        public int getDriverID() { return this.DriverID; }
        public void setDriverID(int DriverID) { this.DriverID = DriverID; }

        //get set price
        public int getPriceID() { return this.PriceID; }
        public void setPriceID(int p) { this.PriceID = p; }

        public int getBusID() { return this.BusID; }
        public void setBusID(int BusID) { this.BusID = BusID; }

        public string getType() { return this.Type; }
        public void setType(string type) { this.Type = type; }
    }
}
