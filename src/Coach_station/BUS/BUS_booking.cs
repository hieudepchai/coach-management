﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
using System.Collections;
namespace BUS
{
    public class BUS_Booking
    {
        DAL.DAL_Booking DALBooking = new DAL.DAL_Booking();
        public bool addBooking(DTO.DTO_Booking book)
        {
            return DALBooking.addBooking(book);
        }
        public int deleteBooking(int id)
        {
            return DALBooking.deleteBus(id);
        }
        public int editBooking(DTO_Booking booking)
        {
            return DALBooking.editBooking(booking);
        }

        public DataTable getBooking()
        { return DALBooking.getBooking();
        }

        public List<int> isSelectedSeat(int TripID)
        {
            return DALBooking.isSelectedSeat(TripID);
        }
        public List<Tuple<DTO_Passenger, int>> getPsgandSeatList(int TripID)
        {
            return DALBooking.getPsgandSeatList(TripID);
        }
    }
}