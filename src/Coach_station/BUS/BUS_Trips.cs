﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class BUS_Trips
    {
        DAL.DAL_Trips DAL_trip = new DAL.DAL_Trips();

        public List<DTO.TripsInfor> getListTripsInfor(string date, string DeptPlace)
        {
            return DAL_trip.getListTripsInfor(date, DeptPlace);
        }

        public int getTripID(string DepTime, string DepPlace, string date)
        {
            return DAL_trip.getTripID(DepTime, DepPlace, date);
        }
    }
}
