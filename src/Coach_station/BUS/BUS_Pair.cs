﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
namespace BUS
{
    public class BUS_Pair
    {
        DAL_Pair dal_pair = new DAL_Pair();
        public DataTable getPair()
        {
            return dal_pair.getPair();
        }
        public List<Tuple<string,string>> getTupleList()
        {
            return dal_pair.getTupleList();
        }
        public int editPair(DTO_Pair pair)
        {
            return dal_pair.editPair(pair);
        }
    }
}
