﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
using System.Collections;


namespace BUS
{
    public class BUS_Bus
    {
        DAL_Bus DALbus = new DAL_Bus();
        public bool addBus(DTO_Bus bus)
        {
            return DALbus.addBus(bus);
        }
        public int deleteBus(int id)
        {
            return DALbus.deleteBus(id);
        }
        public int editBus(DTO_Bus bus)
        {
            return DALbus.editBus(bus);
        }

        public DataTable getBus()
        { return DALbus.getBus(); }

    }
}
