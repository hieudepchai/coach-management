﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
using System.Collections;


namespace BUS
{
    public class BUS_Account
    {
        DAL_Account DALaccount = new DAL_Account();
        public bool addAccount(DTO_Account account)
        {
            return DALaccount.addAccount(account);
        }
        public int deleteAccount(int id)
        {
            return DALaccount.deleteAccount(id);
        }
        public int editAccount(DTO_Account account)
        {
            return DALaccount.editAccount(account);
        }
        public DTO_Account checkAccount(string userN, string pass)
        {
            return DALaccount.checkAccount(userN, pass);
        }
    }
}
