﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
using System.Collections;

namespace BUS
{
    public class BUS_Trip
    {
        DAL_Trip DALtrip = new DAL_Trip();

        public bool addTrip(DTO_Trip trip)
        {
            return DALtrip.addTrip(trip);
        }
        public void generateDailyTrips(string date) {
            DALtrip.generateDailyTrips(date);
        }
        public List<Tuple<string,string, int, DTO_Trip,int>> loadTrip_PlaceDate(string place, string date)
        {
            return DALtrip.loadTrip_PlaceDate(place, date);
        }
    }
}
