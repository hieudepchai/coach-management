﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class Finance_analysis
    {
        DAL.Finance_analysis FinanceAnalysis = new DAL.Finance_analysis();
        public List<DTO.Finance_analysis> getnFinance(string Startdate, string EndDate)
        {
            return FinanceAnalysis.getnFinance(Startdate, EndDate);
        }

        public List<string> getListTimes()
        {
            return FinanceAnalysis.getTime();
        }
    }
}
