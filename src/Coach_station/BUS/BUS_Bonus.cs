﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
using System.Collections;

namespace BUS
{
    public class BUS_Bonus
    {
        DAL_Bonus DALBonus = new DAL_Bonus();
        public bool addBonus(DTO_Bonus bns)
        {
            return DALBonus.addBonus(bns);
        }
        public int deleteBonus(int id)
        {
            return DALBonus.deleteBus(id);
        }
        public int editBonus(DTO_Bonus bns)
        {
            return DALBonus.editBus(bns);
        }

        public DataTable getBonus()
        { return DALBonus.getBonus(); }
    }

}

