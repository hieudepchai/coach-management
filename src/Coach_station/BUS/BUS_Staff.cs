﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
using System.Collections;

namespace BUS
{
    public class BUS_Staff
    {

            DAL_Staff DALstaff = new DAL_Staff();
            public DataTable getStaff()
            { return DALstaff.getStaff(); }
            public DataTable getDriver()
            { return DALstaff.getDriver(); }

        public bool addStaff(DTO_Staff staff)
            {
                return DALstaff.addStaff(staff);
            }
            public int deleteStaff(int id)
            {
                return DALstaff.deleteStaff(id);
            }
            public int editStaff(DTO_Staff staff)
            {
                return DALstaff.editStaff(staff);
            }
        public int getStaffID(string name)
        {
            return DALstaff.getStaffID(name);
        }
        
    }
}

