﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using DTO;
using System.Collections;

namespace BUS
{
    public class BUS_Passenger
    {
        DAL_Passenger DALpassenger = new DAL_Passenger();
        public bool addPassenger(DTO_Passenger passenger)
        {
            return DALpassenger.addPassenger(passenger);
        }
        public int deletePassenger(int id)
        {
            return DALpassenger.deletePassenger(id);
        }
        public int editPassenger(DTO_Passenger passenger)
        {
            return DALpassenger.editPassenger(passenger);
        }

        public DataTable getPassenger()
        { return DALpassenger.getPassenger(); }

        public int getExistPassengerID(string phoneN)
        {
            return DALpassenger.getExistPassengerID(phoneN);
        }
    }
}
