
drop database coach_station;
create database coach_station;

drop table Account;

create table Passenger (
	PassengerID int IDENTITY(1,1),
	Fullname nvarchar(255),
	BirthYear int,
	PhoneNumber nvarchar(10) unique,
	CreatedDate date,
	constraint pk_passenger primary key(PassengerID)
);


create table Bus(
	BusID int IDENTITY(1,1),
	LiscencePlate nvarchar(9),
	BrandName nvarchar(255),
	BoughtDate date,
	constraint pk_bus primary key(BusID)
);
create table Position(
	PositionID int IDENTITY(1,1),
	PositionName nvarchar(255),
	BasicSalary float,
	constraint pk_position primary key(PositionID)
);

create table Staff(
	StaffID int IDENTITY(1,1),
	FullName nvarchar(255),
	DOB date,
	PhoneNumber nvarchar(10),
	Address nvarchar(255),
	PositionID int,
	StartDate date,
	Salary float,
	constraint pk_staff primary key(StaffID),
	constraint fk_staff_positionid foreign key (PositionID) references Position(PositionID)
);


create table Account(
	AccountID int IDENTITY(1,1),
	Username nvarchar(255) unique,
	Password nvarchar(500),
	StaffID int,
	constraint pk_account primary key (AccountID),
	constraint fk_account_staffid foreign key (StaffID) references Staff(StaffID) 
);


create table TicketPrice(
	PriceID int IDENTITY(1,1),
	StartDate date,
	Price int,
	constraint pk_TicketPrice primary key (PriceID)
);

create table Trip(
	TripID int IDENTITY(1,1) not null,
	DepartPlace nvarchar(255) not null,
	DepartureTime nvarchar(5) not null,
	Date date not null,
	PriceID int,
	Driver int,
	BusID int,
	Type nvarchar(20),
	constraint pk_trip primary key (TripID),
	constraint cons_unique unique(DepartPlace, DepartureTIme, Date),
	constraint fk_trip_driver foreign key (Driver) references Staff(StaffID),
	constraint fk_trip_busid foreign key (BusID) references Bus(BusID),
	constraint fk_trip_TicketPrice foreign key (PriceID) references TicketPrice(PriceID)
);
drop table trip;

alter table booking
add constraint fk_booking_tripid; 

alter table Trip
drop constraint pk_trip;

alter table Trip
add constraint pk_trip primary key (TripID,DepartPlace,DepartureTime,Date);




create table Booking(
	TripID int,
	SeatNo int,
	PassengerID int,
	CreatedDate date,
	constraint pk_booking primary key (TripID, SeatNo),
	constraint fk_booking_tripid foreign key(TripID) references Trip(TripID),
	constraint fk_booking_passengerid foreign key(PassengerID) references Passenger(PassengerID)
);



select* from Passenger;



select count(*) from Booking where TripID = 1;

select id from trip where Date ='2019/01/18';


insert into Booking values(1,4,1,'2019/01/18');
insert into Booking values(1,5,1,'2019/01/18');
insert into Booking values(1,25,2,'2019/01/18');
insert into Booking values(4,12,3,'2019/01/18');
insert into Booking values(4,15,5,'2019/01/18');



create table Bonus(
	BonusID int IDENTITY(1,1),
	StaffID int,
	BonusAmount float,
	ValidDate date,
	Description nvarchar(255),
	constraint pk_bonus primary key(BonusID),
	constraint fk_bonus_staffid foreign key(StaffID) references Staff(StaffID)
)

create table Pair(
	PairID int,
	StaffID int unique,
	BusID int unique,
	constraint pk_pair primary key (PairID),
	constraint fk_pair_staffid foreign key (StaffID) references Staff(StaffID),
	constraint fk_pair_busid foreign key (BusID) references Bus(BusID)
)

alter table Pair
add constraint pair_unique_staffid unique(StaffID);


alter table Pair
add constraint pair_unique_busid unique(BusID);

select* from TicketPrice;
insert into TicketPrice values('2019/1/1',65000);


insert into Pair values('1','1','1');
insert into Pair values('2','2','2');
insert into Pair values('3','3','3');
insert into Pair values('4','4','4');
insert into Pair values('5','5','5');
insert into Pair values('6','6','6');
insert into Pair values('7','7','7');
insert into Pair values('8','8','8');
insert into Pair values('9','9','9');
insert into Pair values('10','10','10');
insert into Pair values('11','11','11');
insert into Pair values('12','12','12');
insert into Pair values('13','13','13');
insert into Pair values('14','14','14');
insert into Pair values('15','15','15');
insert into Pair values('16','16','16');
select StaffID, BusID from Pair;


/*
create table TripDefault(
	TripDfID int IDENTITY(1,1),
	DepartPlace nvarchar(255),
	DepartureTime nvarchar(4),
	Type nvarchar(20),
	Driver int,
	BusID int,
	constraint pk_tripDf primary key (TripDfID),
	constraint fk_tripDf_driver foreign key (Driver) references Staff(StaffID),
	constraint fk_tripDf_busid foreign key (BusID) references Bus(BusID)
);*/

select* from Bus;
select* from Staff where PositionID='2';



select* from trip;
delete from trip;

select p.PassengerID, b.SeatNo, p.FullName,p.BirthYear, p.CreatedDate from Booking b, Passenger p where TripID = '1' and p.PassengerID = b.PassengerID;


select* from trip where TripID = 63;

insert into Passenger values('Nguyen Minh Hieu',1998,'12456789','2019/01/01');
insert into Passenger values('Tran A', 1995,'01010101','2018/03/03');
insert into Passenger values('Tran B',1992,'0202020202','2018/12/12');
insert into Passenger values('Le C', 1980,'0404040404','2018/10/20');



insert into Bus values('51B-00001','Hyundai','2017-10-12');
  insert into Bus values('51B-00002','Hyundai','2017-10-12');
  insert into Bus values('51B-00003','Hyundai','2017-10-12');
  insert into Bus values('51B-00004','Hyundai','2017-10-12');
  insert into Bus values('51B-00005','Hyundai','2017-10-12');

  insert into Bus values('59B-00006','Hyundai','2018-03-20');
  insert into Bus values('59B-00007','Hyundai','2018-03-20');
  insert into Bus values('59B-00008','Hyundai','2018-03-20');
  insert into Bus values('59B-00009','Hyundai','2018-03-20');
  insert into Bus values('59B-00010','Hyundai','2018-03-20');

  insert into Bus values('52B-00011','Hyundai','2018-06-30');
  insert into Bus values('52B-00012','Hyundai','2018-06-30');
  insert into Bus values('52B-00013','Hyundai','2018-06-30');
  insert into Bus values('52B-00014','Hyundai','2018-06-30');
  insert into Bus values('52B-00015','Hyundai','2018-06-30');

  insert into Bus values('51B-00016','Hyundai','2019-03-21');
  insert into Bus values('51B-00017','Hyundai','2019-03-21');
  insert into Bus values('51B-00018','Hyundai','2019-03-21');
  insert into Bus values('51B-00019','Hyundai','2019-03-21');
  insert into Bus values('51B-00020','Hyundai','2019-03-21');

  insert into Bus values('55B-00021','Hyundai','2019-06-15');
  insert into Bus values('55B-00022','Hyundai','2019-06-15');
  insert into Bus values('55B-00023','Hyundai','2019-06-15');
  insert into Bus values('55B-00024','Hyundai','2019-06-15');
  insert into Bus values('55B-00025','Hyundai','2019-06-15');

  insert into Bus values('59B-00026','Hyundai','2019-09-17');
  insert into Bus values('59B-00027','Hyundai','2019-09-17');
  insert into Bus values('59B-00028','Hyundai','2019-09-17');
  insert into Bus values('59B-00029','Hyundai','2019-09-17');
  insert into Bus values('59B-00030','Hyundai','2019-09-17');

  insert into Position values('Staff',10000000);
  insert into Position values('Driver',20000000);

  insert into Staff values('Tran Thanh B','1977-10-20','0919123111','115B/23/17 Ngo gia tu',2,'2015-10-30',20000000);
  insert into Staff values('Nguyen Thanh Luong','1975-11-12','0817927723','23 Nguyen Du',2,'2014-11-03',20000000);
  insert into Staff values('Truong Van Thanh','1970-10-20','0974127659','1109 Ly Thuong Kiet',2,'2016-10-30',20000000);
  insert into Staff values('Tran Thanh Trong','1977-11-12','0913873265','709/12 Chau Van Liem',2,'2016-11-03',20000000);
  insert into Staff values('Vo Van Khuan','1987-11-22','0912345689','778/15B/12 Hoan Dieu',2,'2017-05-03',20000000);
  
  insert into Staff values('Tran Thanh Trong','1975-11-12','0817927723','23 Nguyen Du',2,'2014-11-03',20000000);
  insert into Staff values('Truong Van Si','1970-10-20','0974127659','1109 Ly Thuong Kiet',2,'2016-10-30',20000000);
  insert into Staff values('Vo Thanh Khiet','1977-11-12','0913873265','709/12 Chau Van Liem',2,'2016-11-03',20000000);
  insert into Staff values('Nguyen Luong Khiem','1970-10-25','0791238919','899 Nguyen Van Banh',2,'2015-12-05',20000000);
  insert into Staff values('Hoang Thanh An','1979-09-27','0912345689','778/15B/12 Nguyen Trai',2,'2017-01-09',20000000);


  insert into Staff values('Tran Thanh Bao','1979-01-15','0997817928','11/2 Van Van Kiet',2,'2015-08-30',20000000);
  insert into Staff values('Nguyen Thanh Luong','1980-02-12','089791732','1 Dong Khoi',2,'2015-09-03',20000000);
  insert into Staff values('Truong Van Thanh','1981-03-16','0812839423','1209 Ly Thuong Kiet',2,'2015-10-30',20000000);
  insert into Staff values('Tran Thanh Trong','1982-04-17','091838232','711/12 Chau Van Liem',2,'2015-11-03',20000000);
  insert into Staff values('Vo Van Khuan','1983-05-28','0192837271','710/15B/12 Hoan Dieu',2,'2015-05-03',20000000);
  
  insert into Staff values('Nguyen Trong Hoang','1984-11-12','0918272361','778 Lam Van Ben',2,'2018-11-03',20000000);
  insert into Staff values('Truong Van Khiem','1986-10-20','018273625','1109 Cach Mang Thang 8',2,'2018-10-30',20000000);
  insert into Staff values('Vo Thanh Son','1987-11-12','0172819262','709/12 Hoa Hao',2,'2018-11-03',20000000);
  insert into Staff values('Nguyen Luong Danh','1980-10-25','0192836272','899 Nguyen Tri Phuong',2,'2018-12-05',20000000);
  insert into Staff values('Hoang Thien Ton','1989-09-27','0917326161','405 Nguyen Huu Tho',2,'2018-01-09',20000000);


  insert into Staff values('Tran Thanh B','1977-10-20','0919123111','115B/23/17 Ngo gia tu',2,'2015-10-30',20000000);
  insert into Staff values('Nguyen Thanh Luong','1975-11-12','0817927723','23 Nguyen Du',2,'2014-11-03',20000000);
  insert into Staff values('Truong Van Thanh','1970-10-20','0974127659','1109 Ly Thuong Kiet',2,'2016-10-30',20000000);
  insert into Staff values('Tran Thanh Trong','1977-11-12','0913873265','709/12 Chau Van Liem',2,'2016-11-03',20000000);
  insert into Staff values('Vo Van Khuan','1987-11-22','0912345689','778/15B/12 Hoan Dieu',2,'2017-05-03',20000000);
  
  insert into Staff values('Tran Thanh Trong','1975-11-12','0817927723','23 Nguyen Du',2,'2014-11-03',20000000);
  insert into Staff values('Truong Van Si','1970-10-20','0974127659','1109 Ly Thuong Kiet',2,'2016-10-30',20000000);
  insert into Staff values('Vo Thanh Khiet','1977-11-12','0913873265','709/12 Chau Van Liem',2,'2016-11-03',20000000);
  insert into Staff values('Nguyen Luong Khiem','1970-10-25','0791238919','899 Nguyen Van Banh',2,'2015-12-05',20000000);
  insert into Staff values('Hoang Thanh An','1979-09-27','0912345689','778/15B/12 Nguyen Trai',2,'2017-01-09',20000000);




		select count(*) as Num from Booking where TripID = '1';
